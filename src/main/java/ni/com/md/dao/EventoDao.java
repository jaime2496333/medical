/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.Date;
import java.util.List;

import ni.com.md.domain.Evento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface EventoDao extends CrudRepository<Evento, Long> {

    @Query("select e,u from Evento e join fetch e.ubicacion u")
    public List<Evento> listarEventos();
    
    @Query("select e,u from Evento e join fetch e.ubicacion u where e.fecha_hora between ?1 and ?2 and u.id_ubicacion = ?3 and u.activo=1 "
            + "and e.activo=1 order by e.fecha_hora")
    public List<Evento> listarEventosPorFecha(Date inicio, Date fin, Long id_ubicacion);

    @Query("select e,c,u from Evento e "
            + "join e.cursos c "
            + "join e.ubicacion u "
            + "where e.publicado = 1 and e.activo=1 and c.id_curso in (?1) and e.fecha_hora between ?2 and ?3 "
            + "and (u.nombre_ubicacion like %?4% or "
            + "u.calle like %?4% or "
            + "u.colonia like %?4% or "
            + "u.ciudad like %?4%) "
            + "order by e.fecha_hora")
    public List<Evento> listarEventosPorCursoPagadoDelAlumno(Long[] cursosPagados, Date fechaInicio, Date fechaFin, String keyword);
    
    
    @Query("select e from Evento e "
            + "join fetch e.ubicacion u "
            + "join fetch e.maestro m "
            + "where e.fecha_hora between ?1 and ?2 "
            + "and m.id_usuario = ?3 and e.activo=1 and u.activo = 1 order by e.fecha_hora")
    public List<Evento> listarEventosPorProfesor(Date inicio, Date fin, Long id_profesor);
    
    @Query("select e from Evento e "
            + "where e.fecha_hora > ?1 ")
    public List<Evento> listarEventosFuturos(Date inicio, Long id_calendario);

}
