/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Material;

public interface MaterialService {

    public List<Material> listarMateriales();

    public void guardar(Material material);

    public void eliminar(Material material);

    public Material encontrarMaterial(Material material);
   
    public Material encontrarMaterialPorId(Long id_material);

    public List<Material> listarMaterialesActivos();

}
