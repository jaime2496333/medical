/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.MaterialCursoDao;
import ni.com.md.domain.MaterialCurso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lenovo-T430-DT
 */
@Service
public class MaterialCursoServiceImp implements MaterialCursoService {

    @Autowired
    MaterialCursoDao materialCursoDao;

    @Override
    @Transactional(readOnly = true)
    public List<MaterialCurso> listarMaterialesPorCurso(Long id_curso) {
        return materialCursoDao.listarMaterialesPorCurso(id_curso);
    }

    @Override
    @Transactional
    public void eliminar(MaterialCurso materialCurso) {
        materialCursoDao.delete(materialCurso);
    }

    @Override
    @Transactional
    public void guardar(MaterialCurso materialCurso) {
        materialCursoDao.save(materialCurso);
    }

    @Override
    @Transactional(readOnly = true)
    public MaterialCurso encontrarMaterialCurso(MaterialCurso materialCurso) {
        return materialCursoDao.findById(materialCurso.getId_material_curso()).orElse(null);
    }

}
