/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import java.util.Objects;
import ni.com.md.dao.CuentaDao;
import ni.com.md.dao.InscripcionDao;
import ni.com.md.dao.NivelCuentaDao;
import ni.com.md.domain.Cuenta;
import ni.com.md.domain.Inscripcion;
import ni.com.md.domain.NivelCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author julio
 */
@Service

public class CuentaServiceImpl implements CuentaService {

    @Autowired
    private CuentaDao cuentaDao;

    @Autowired
    private NivelCuentaDao nivelCuentaDao;

    @Autowired
    private InscripcionDao inscripcionDao;

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> listarCuentas() {
        return (List<Cuenta>) cuentaDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Cuenta cuenta) {
        cuentaDao.save(cuenta);
        if (Objects.equals(cuenta.getLiberado(), Boolean.TRUE)) {
            for (NivelCuenta nivelCuenta : cuenta.getNivelesCuentas()) {
                nivelCuenta.setLiberado(Boolean.TRUE);
                nivelCuentaDao.save(nivelCuenta);

                Inscripcion inscripcion = nivelCuenta.getInscripcion();
                inscripcion.setLiberado(Boolean.TRUE);
                inscripcionDao.save(inscripcion);
            }
        }
    }

    @Override
    @Transactional
    public void eliminar(Cuenta cuenta) {
        cuentaDao.delete(cuenta);
    }

    @Override
    @Transactional(readOnly = true)
    public Cuenta encontrarCuenta(Cuenta cuenta) {
        return cuentaDao.findById(cuenta.getId_cuenta()).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> listarCuentasSinPagar(String term) {
        return cuentaDao.listarCuentasSinPagar(term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> listarCuentasDeInscripcionSinPagar(String term) {
        return cuentaDao.listarCuentasDeInscripcionSinPagar(term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cuenta> listarCuentasPagadasPorCurso(Long id_curso) {
        return cuentaDao.listarCuentasPagadasPorCurso(id_curso);
    }

}
