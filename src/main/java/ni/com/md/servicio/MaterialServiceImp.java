/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.dao.MaterialDao;
import ni.com.md.domain.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MaterialServiceImp implements MaterialService {
    
    @Autowired
    private MaterialDao materialDao;
    
    @Override
    @Transactional(readOnly = true)
    
    public List<Material> listarMateriales() {
        return (List<Material>) materialDao.findAll();
    }
    
    @Override
    @Transactional
    
    public void guardar(Material material) {
        material.setActivo(Boolean.TRUE);
        materialDao.save(material);
    }
    
    @Override
    @Transactional
    
    public void eliminar(Material material) {
        Material materialDB = materialDao.findById(material.getId_material()).orElse(null);
        if (materialDB != null) {
            materialDB.setActivo(Boolean.FALSE);
            materialDao.save(materialDB);
        }
        
    }
    
    @Override
    @Transactional(readOnly = true)
    
    public Material encontrarMaterial(Material material) {
        // return categoriaDao.findById(categoria.getId_categoria()).orElse(null);
        Material materialDB = materialDao.findById(material.getId_material()).orElse(null);
        if (materialDB == null) {
            return null;
        }
        return materialDB;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Material> listarMaterialesActivos() {
        return materialDao.listarMaterialesActivos();
    }
    
    @Override
    public Material encontrarMaterialPorId(Long id_material) {
        return materialDao.findById(id_material).orElse(null);
    }
    
}
