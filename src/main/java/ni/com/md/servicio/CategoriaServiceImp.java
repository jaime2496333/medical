/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.CategoriaDao;
import ni.com.md.domain.Categoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoriaServiceImp implements CategoriaService {

    @Autowired
    private CategoriaDao categoriaDao;

    @Override
    @Transactional(readOnly = true)
   
    public List<Categoria> listarCategorias() {
        return (List<Categoria>) categoriaDao.findAll();
    }
    
    
   
    

    @Override
    @Transactional

    public void guardar(Categoria categoria) {
        categoria.setActivo(Boolean.TRUE);
        categoriaDao.save(categoria);
    }

    @Override
    @Transactional

    public void eliminar(Categoria categoria) {
       Categoria categoriaDB = categoriaDao.findById(categoria.getId_categoria()).orElse(null);
        if (categoriaDB != null) {
            categoriaDB.setActivo(Boolean.FALSE);
       categoriaDao.save(categoriaDB);
        }
        
       
    }

    @Override
    @Transactional(readOnly = true)

    public Categoria encontrarCategoria(Categoria categoria) {
       // return categoriaDao.findById(categoria.getId_categoria()).orElse(null);
       Categoria categoriaDB = categoriaDao.findById(categoria.getId_categoria()).orElse(null);
        if (categoriaDB == null) {
            return null;
        }
        return categoriaDB;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Categoria> listarCategoriasActivas() {
      return  categoriaDao.listarCategoriasActivas();
    }

}
