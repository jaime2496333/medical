/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import java.util.Date;

@Data
@Entity
@Table(name = "clase_alumno")
public class ClaseAlumno implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_clase_alumno;

    private int  numero_clases;

    private int clases_tomadas;
    
    private Date fecha_proxima_clase;
    
    @ManyToOne
    @JoinColumn(name = "id_alumno")
    private Usuario alumno;
    
     @ManyToOne
    @JoinColumn(name = "id_curso")
    private Curso curso;
}
