/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Abono;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface AbonoService {
     public List<Abono> listarAbonos();
    
    public void guardar (Abono abono);
    
    public void eliminar (Abono abono);
    
    public Abono encontrarAbono(Abono abono); 
    
}
