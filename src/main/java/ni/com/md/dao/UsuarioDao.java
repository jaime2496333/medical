/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;

import java.util.List;

import ni.com.md.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsuarioDao extends JpaRepository<Usuario, Long>{
    Usuario findByUsername(String username);
    
     @Query("select distinct u,r from Usuario u join fetch u.roles r where u.activo=1 and r.nombre in (?1)")
    public List<Usuario> listarUsuariosPorRol(String[] term);
    
    @Query("select distinct u,r from Usuario u join fetch u.roles r where u.activo=1 and u.id_usuario=?1")
    public Usuario buscarUsuarioPorId(Long id);
    
     @Query("select distinct u from Usuario u  where u.activo=1 and u.username =?1")
    public Usuario listarPropiedadesDeUsuario(String term);
    
    @Query("select distinct u,r "
               + "from Usuario u "
               + "join fetch u.roles r "
               + "where u.activo=1 and r.nombre in (?1) and (u.username like %?2% or u.nombres like %?2% or u.apellidos like %?2% or u.correo like %?2%)")
    public List<Usuario> listarUsuarioPorUserName(String[] term, @Param("keyword") String keyword);
    
    @Query("select distinct us from Usuario us "
            + "join fetch us.inscripciones ins "
            + "join fetch ins.nivel ni "
            + "join fetch ins.curso cur "
            + "where cur.id_curso=?1 "
            + "and (ni.descripcion_nivel like %?2% or "
            + "us.username like %?2% or "
            + "us.nombres like %?2% or "
            + "us.apellidos like %?2%)")
    public List<Usuario> listarUsuariosPorCursoYNivel(Long id_curso,  String term);
    
}
