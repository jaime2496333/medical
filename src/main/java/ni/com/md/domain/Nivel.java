/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;


@Data
@Entity
@Table(name="nivel")
public class Nivel {
    private static final Long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_nivel;
    
    private String nombre_nivel;

    private String descripcion_nivel;

    private float precio_unitario;
  
    private float precio_curso_completo;
    
    private Boolean activo;

    @ManyToOne
    @JoinColumn(name="id_curso")
    private Curso curso;

  
    
    
}
