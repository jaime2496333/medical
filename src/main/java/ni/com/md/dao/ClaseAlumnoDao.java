/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;

import ni.com.md.domain.ClaseAlumno;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface ClaseAlumnoDao extends CrudRepository<ClaseAlumno, Long>{
    
   @Query("select c "
           + "from ClaseAlumno c "
           + "join fetch c.alumno a "
           + "join fetch c.curso cur "
           + "where a.id_usuario = ?1 and cur.id_curso = ?2"
   )
    public ClaseAlumno buscarPorAlumnoYCurso(Long id_alumno, Long id_Curso);
    
    
    @Query("select c "
           + "from ClaseAlumno c "
           + "join fetch c.alumno a "
           + "join fetch c.curso cur "
           + "where a.id_usuario = ?1"
   )
    public List<ClaseAlumno> buscarPorAlumno(Long id_alumno);
}

