/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "comision")
public class Comision implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_comision;
    
    private float monto_comision;
    
    private Date fecha_creacion;
    
    @ManyToOne
    @JoinColumn(name = "id_alumno")
    private Usuario alumno;
    
    
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
    
    @ManyToOne
    @JoinColumn(name = "id_curso")
    private Curso curso;
    
    
     @PrePersist
    public void fechaDefault() {
        fecha_creacion = new Date();
    }
}
