/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.CalendarioDao;
import ni.com.md.dao.UbicacionDao;
import ni.com.md.domain.Calendario;
import ni.com.md.domain.Evento;
import ni.com.md.domain.Ubicacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author julio
 */
@Service
public class UbicacionServiceImp implements UbicacionService {

    @Autowired
    private UbicacionDao ubicacionDao;
    
    @Autowired
    private CalendarioDao calendarioDao;
    
    @Override
    @Transactional(readOnly = true)
    public List<Ubicacion> listarUbicaciones() {
       return (List<Ubicacion>)ubicacionDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Ubicacion ubicacion) {
        ubicacionDao.save(ubicacion);
    }
    
    
    @Override
    @Transactional
    public void eliminar(Ubicacion ubicacion) {
        Ubicacion ubicacionDB = ubicacionDao.findById(ubicacion.getId_ubicacion()).orElse(null);
        if(ubicacionDB != null)
        {
            ubicacionDB.setActivo(Boolean.FALSE);
            ubicacionDao.save(ubicacionDB);
            
            List<Calendario> calendariosDeLaUbicacion = calendarioDao.listarEventosPorUbicacion(ubicacion.getId_ubicacion());
            for(Calendario cal : calendariosDeLaUbicacion)
            {
                for(Evento ev : cal.getEventos())
                {
                    ev.setPublicado(Boolean.FALSE);
                }
                cal.setPublicado(Boolean.FALSE);
            }
            calendarioDao.saveAll(calendariosDeLaUbicacion);
        }
        
    }

    @Override
    @Transactional(readOnly = true)
    public Ubicacion encontrarUbicacion(Ubicacion ubicacion) {
    Ubicacion ubicacionDB = ubicacionDao.findById(ubicacion.getId_ubicacion()).orElse(null);
    if(ubicacionDB == null)
    {
        return null;
    }
        return ubicacionDB;
    }

    @Override
    public List<Ubicacion> listarUbicacionesActivas(String term) {
        return  ubicacionDao.listarUbicacionesActivas(term);
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
