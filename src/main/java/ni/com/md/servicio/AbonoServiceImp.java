/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.AbonoDao;
import ni.com.md.domain.Abono;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AbonoServiceImp implements AbonoService {
    
    @Autowired
    private AbonoDao abonoDao;
    
    @Override
    @Transactional(readOnly = true)
    public List<Abono> listarAbonos() {
        return (List<Abono>) abonoDao.findAll();
    }
    
    @Override
    @Transactional
    public void guardar(Abono abono) {
        abonoDao.save(abono);
    }
    
    @Override
    @Transactional
    public void eliminar(Abono abono) {
        abonoDao.delete(abono);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Abono encontrarAbono(Abono abono) {
        return abonoDao.findById(abono.getId_abono()).orElse(null);
    }
    
}
