/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.domain;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "inscripcion")
public class Inscripcion {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_inscripcion;

    private String modalidad_pago;

    private Date fecha_inscripcion;

    private float precio;

    private float abono;

    private Boolean liberado;
    
    private Boolean activo;

    private  int clases_pagadas;
    
    private int clases_tomadas;
    
    private Date fecha_proxima_agenda;
    
    @PrePersist
    public void fechaDefault()
    {
        fecha_inscripcion = new Date();
    }
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    
    
    @ManyToOne
    @JoinColumn(name = "id_curso")
    private Curso curso;
    
    @ManyToOne
    @JoinColumn(name = "id_nivel")
    private Nivel nivel;
    
}
