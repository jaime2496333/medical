/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Categoria;

public interface CategoriaService {

    public List<Categoria> listarCategorias();

    public void guardar(Categoria categoria);

    public void eliminar(Categoria categoria);

    public Categoria encontrarCategoria(Categoria categoria);

    public List<Categoria> listarCategoriasActivas();

}
