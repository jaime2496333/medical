/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.domain;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "agenda")
public class Agenda implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_agenda;

    private Date fecha_creacion;

    private Date fecha_asistencia;
    
    private Boolean asistencia;
    
    

    @PrePersist
    public void fechaDefault() {
        fecha_creacion = new Date();
        asistencia = Boolean.FALSE;
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario alumno;

    @ManyToOne
    @JoinColumn(name = "id_evento")
    private Evento evento;

    @ManyToOne
    @JoinColumn(name = "id_tema")
    private Tema tema;
    
    @ManyToOne
    @JoinColumn(name = "id_profesor")
    private Usuario profesor;
    
    @ManyToOne
    @JoinColumn(name = "id_ubicacion")
    private Ubicacion ubicacion;
    
    
}
