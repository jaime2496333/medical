/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;


import java.util.List;

import ni.com.md.domain.MaterialCurso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface MaterialCursoDao  extends CrudRepository<MaterialCurso, Long> {
    @Query("select mc from MaterialCurso mc join fetch mc.material m join fetch mc.curso c where c.id_curso = ?1")
    public List<MaterialCurso> listarMaterialesPorCurso(Long id_curso);
}
