
package ni.com.md.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Bean
    public BCryptPasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder build) throws Exception
    {
        build.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
            http.authorizeRequests()
                    .antMatchers("/")
                    .authenticated()
                    .antMatchers(
                            "/inscripciones/**",
                            "/crearClase/**",
                            "/cursos/**", 
                            "/materiales/**",
                            "/categorias/**",
                            "/divisiones/**",
                            "/usuarios/**",
                            "/alumnos/**",
                            "/vendedores/**",
                            "/pagoInscripcion/**",
                            "/alumnosInscritos/**",
                            "/ubicaciones/**",
                            "/crearEvento/**",
                            "/crearCurso/**",
                            "/configurarCurso/**",
                            "/verMaestroYAlumnosDelEvento/**",
                            "/publicarEvento/**",
                            "/eliminarEvento/**",
                            "/BandejaMaestro/**",
                            "/verAlumnosInscritos/**",
                            "/agendar/**",
                            "/agendas/**",
                            "/crearCalendario/**"
                            )
                        .hasAnyRole("ADMIN","COORDINADOR")
                    .antMatchers(
                            "/agendar/**",
                            "/agendas/**")
                        .hasAnyRole("ALUMNO")
                    .antMatchers(
                            "/BandejaMaestro/**")
                        .hasAnyRole("PROFESOR")
                    .and()
                        .formLogin()
                        .loginPage("/login")
                    .and()
                        .exceptionHandling().accessDeniedPage("/errores/403")
                    ;
    }
    
}
