/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Curso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursoDao extends CrudRepository<Curso, Long>  {
    
    @Query("select c,cat,div from Curso c join fetch c.categoria cat "
            + "join fetch c.division div "
            + "where c.activo=1")
    public List<Curso> listarCursosActivos();
    
    
 
    
    
    

}
