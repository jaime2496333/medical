/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.domain.Nivel;


public interface NivelService {

    public List<Nivel> listarNiveles();

    public void guardar(Nivel nivel);

    public void guardarNivel(Nivel nivel, Long id_curso);

    public void eliminar(Nivel nivel);

    public Nivel encontrarNivel(Nivel nivel);

    public List<Nivel> listarNivelesActivosPorCurso(Long id_curso);
    
     public Nivel buscarNivelPorId(Long id_nivel);
     public List<Nivel> listarNivelesActivos();

}
