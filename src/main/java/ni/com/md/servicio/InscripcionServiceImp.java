package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.InscripcionDao;
import ni.com.md.dao.TemaDao;

import ni.com.md.domain.Inscripcion;
import java.util.Date;
import ni.com.md.dao.ClaseAlumnoDao;
import ni.com.md.domain.ClaseAlumno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InscripcionServiceImp implements InscripcionService {

    @Autowired
    private InscripcionDao inscripcionDao;

    @Autowired
    private TemaDao temaDao;

    @Autowired
    private ClaseAlumnoDao claseAlumnoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> listarInscripcion() {
        return (List<Inscripcion>) inscripcionDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Inscripcion inscripcion) {

        int clasesPagadas = temaDao.listarTPorNivel(inscripcion.getNivel().getId_nivel()).size();
        inscripcion.setClases_pagadas(clasesPagadas);
        inscripcion.setClases_tomadas(0);
        inscripcion.setFecha_proxima_agenda(new Date());

        ClaseAlumno claseAlumno = claseAlumnoDao.buscarPorAlumnoYCurso(inscripcion.getUsuario().getId_usuario(),
                inscripcion.getCurso().getId_curso());

        if (claseAlumno == null) {
            claseAlumno = new ClaseAlumno();
            claseAlumno.setAlumno(inscripcion.getUsuario());
            claseAlumno.setCurso(inscripcion.getCurso());
            claseAlumno.setNumero_clases(clasesPagadas);
            claseAlumno.setClases_tomadas(0);
            claseAlumno.setFecha_proxima_clase(new Date());
            claseAlumnoDao.save(claseAlumno);
        } else {
            if(claseAlumno.getClases_tomadas() == 0){
            claseAlumno.setNumero_clases(clasesPagadas + claseAlumno.getNumero_clases());}
        }

        inscripcionDao.save(inscripcion);
    }

    @Override
    @Transactional
    public void eliminar(Inscripcion inscripcion) {
        inscripcionDao.delete(inscripcion);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> listarInscripcionActivas() {
        return (List<Inscripcion>) inscripcionDao.listarInscripcionActivas();
    }

    @Override
    @Transactional(readOnly = true)
    public Inscripcion encontrarInscripcionbyId(Long id_inscripcion) {
        return inscripcionDao.findById(id_inscripcion).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public Inscripcion encontrarInscripcion(Inscripcion inscripcion) {
        Inscripcion inscripcionDB = inscripcionDao.findById(inscripcion.getId_inscripcion()).orElse(null);
        if (inscripcionDB == null) {
            return null;
        }
        return inscripcionDB;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> listarInscripcionActivasPorCursoNivel(Long id_curso, String term) {
        return inscripcionDao.listarInscripcionActivasPorCursoNivel(id_curso, term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> buscarInscripcionPorUsuarioYNivel(Long id_usuario, Long[] niveles) {
        return inscripcionDao.buscarInscripcionPorUsuarioYNivel(id_usuario, niveles);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> listarInscripcionesSinPagar(String term) {
        return inscripcionDao.listarInscripcionesSinPagar(term);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> buscarInscripcionesPagadasPorAlumno(Long id_alumno) {
        return inscripcionDao.buscarInscripcionesPagadasPorAlumno(id_alumno);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> buscarInscripcionesPorAlumno(Long id_alumno) {
        return inscripcionDao.buscarInscripcionesPorAlumno(id_alumno);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Inscripcion> buscarInscripcionPorUsuarioYCurso(Long id_usuario, Long id_curso) {
        return inscripcionDao.buscarInscripcionPorUsuarioYCurso(id_usuario, id_curso);
    }

}
