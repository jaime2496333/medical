/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.domain.MaterialCurso;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface MaterialCursoService {
    public List<MaterialCurso> listarMaterialesPorCurso(Long id_curso);
    public void eliminar(MaterialCurso materialCurso);
    public void guardar(MaterialCurso materialCurso);
    public MaterialCurso encontrarMaterialCurso(MaterialCurso materialCurso);
}
