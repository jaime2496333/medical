/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;
import java.util.List;
import ni.com.md.domain.Division;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DivisionDao extends CrudRepository<Division, Long> {
    
    @Query("select d from Division d where d.activo=1")
    public List<Division> listarDivisionesActivas();
    
}
