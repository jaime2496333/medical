/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Tema;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface TemaService {
    
    public List<Tema> listartemas();

    public void guardar(Tema tema);

    public void guardarTema(Tema tema, Long id_nivel);

    public void eliminar(Tema tema);

    public Tema encontrarTema(Tema tema);

    public List<Tema> listarTemasActivosPorCurso(Long id_curso);
    
    public List<Tema> listarTPorNivel(Long id_Nivel);
}
