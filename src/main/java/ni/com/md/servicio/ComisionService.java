/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Comision;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface ComisionService {
    
    public List<Comision> listarComisiones();
    
    public void guardar (Comision comision);
    
    public void eliminar (Comision comision);
    
    public Comision encontrarAbono(Comision comision); 
}
