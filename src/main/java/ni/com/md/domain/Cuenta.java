package ni.com.md.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "cuenta")

public class Cuenta implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_cuenta;

    Date fecha_cuenta;

    private Boolean liberado;

    private float monto_cuenta;

    private float abonado;
    
    private Boolean es_tipo_inscripcion;
    
    private Date vigencia;
    
    private Date fecha_limite_de_pago;
            

    @PrePersist
    public void valoresDefault() {
        fecha_cuenta = new Date();
        liberado = false;
    }

    @ManyToOne
    @JoinColumn(name = "id_alumno")
    private Usuario alumno;
    
    @ManyToOne
    @JoinColumn(name = "id_curso")
    private Curso curso;

    @ManyToOne
    @JoinColumn(name = "id_usuario_creador")
    private Usuario usuarioCreador;
    
    @ManyToOne
    @JoinColumn(name = "id_vendedor")
    private Usuario vendedor;
    
    @OneToMany
    @JoinColumn(name="id_cuenta")
    private List<NivelCuenta> nivelesCuentas;
    
    @OneToMany
    @JoinColumn(name="id_cuenta")
    private List<Abono> abonos;


}
