package ni.com.md.servicio;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import ni.com.md.dao.UsuarioDao;
import ni.com.md.domain.Rol;
import ni.com.md.domain.Usuario;
import ni.com.md.util.EncriptarPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailService")
@Slf4j
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioDao usuarioDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioDao.findByUsername(username);

        if (usuario == null) {
            throw new UsernameNotFoundException(username);
        }

        var roles = new ArrayList<GrantedAuthority>();

        for (Rol rol : usuario.getRoles()) {
            roles.add(new SimpleGrantedAuthority(rol.getNombre()));
        }
        return new User(usuario.getUsername(), usuario.getPassword(), roles);
    }

    @Transactional(readOnly = true)
    public List<Usuario> listarUsuarios() {
        List<Usuario> usuarios = usuarioDao.findAll();
        return usuarios;
    }

    @Transactional
    public void guardarUsuario(Usuario usuario) {
        Usuario usuarioDB = null;
        if (usuario.getId_usuario() != null) { //El usuario existe en la base de datos
            usuarioDB = usuarioDao.getById(usuario.getId_usuario());
            //validamos si se cambio el password
            if (usuario.getPassword().isEmpty()) {
                usuario.setPassword(usuarioDB.getPassword());

            } else {
                String passwordEscritoPorElUsuario = usuario.getPassword();
                usuario.setPassword(EncriptarPassword.encriptarPassword(passwordEscritoPorElUsuario));
            }
            //Validamos si se cambio la foto
            if (usuario.getfoto() == null) {
                usuario.setfoto(usuarioDB.getfoto());
            }

        } else//El usuario no existe en la base de datos y encriptamos su password 
        {
            String passwordEscritoPorElUsuario = usuario.getPassword();
            usuario.setPassword(EncriptarPassword.encriptarPassword(passwordEscritoPorElUsuario));

        }

        usuario.setActivo(Boolean.TRUE);

        usuarioDao.save(usuario);

    }

    @Transactional
    public void eliminarUsuario(Usuario usuario) {
        usuarioDao.delete(usuario);

    }

    @Transactional
    public Usuario buscarUsuario(Usuario usuario) {
        //return usuarioDao.findById(usuario.getId_usuario()).orElse(null);
        return usuarioDao.buscarUsuarioPorId(usuario.getId_usuario());
    }

    @Transactional
    public List<Usuario> listarUsuarioPorRoles(String[] listaDeRoles) {
        return usuarioDao.listarUsuariosPorRol(listaDeRoles);
    }

    @Transactional(readOnly = true)
    public Usuario listarPropiedadesDeUsuario(String username) {

        return usuarioDao.listarPropiedadesDeUsuario(username);
    }

    @Transactional(readOnly = true)
    public List<Usuario> listarUsuarioPorUserName(String[] listaDeRoles, String keyword) {
        return usuarioDao.listarUsuarioPorUserName(listaDeRoles, keyword);
    }
    
    @Transactional(readOnly = true)
    public List<Usuario> listarUsuariosPorCursoYNivel(Long id_curso,  String term)
    {
        return usuarioDao.listarUsuariosPorCursoYNivel(id_curso, term);
                
    }
}
