
package ni.com.md.domain;

import java.util.List;
import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name="usuario")
public class Usuario {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_usuario;
    

    private String username;
    

    private String password;
    

    private String nombres;

    private String apellidos;
    
    private String telefono;
 
    private String correo;
   
    private String fecha_nacimiento;
    
    private String id_estado_civil;
    
    private String sexo;
    
    private String nombre_contacto;
    
    private String telefono_contacto;
   
    private Boolean activo;

    private String foto;

    private String fecha_ingreso;
    
    private Long sueldo;
    
    private float monto_comision_acumulada;
    
    @OneToMany
    @JoinColumn(name="id_usuario")
    private List<Rol> roles;
    
    @OneToMany
    @JoinColumn(name="id_usuario")
    private List<Inscripcion> inscripciones;
    
    public String getfoto(){
    return foto;
    }
    public void setfoto(String foto){
    this.foto=foto;
    }

}
