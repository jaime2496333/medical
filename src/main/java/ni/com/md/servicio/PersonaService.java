package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Persona;

public interface PersonaService {
    
    public List<Persona> listarPersonas();
    
    public void guardar (Persona persona);
    
    public void eliminar (Persona persona);
    
    public Persona encontrarPersona(Persona persona); 
    
}
