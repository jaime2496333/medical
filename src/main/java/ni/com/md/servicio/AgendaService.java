/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import java.util.Date;
import ni.com.md.domain.Agenda;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface AgendaService {
    
     public List<Agenda> listarAgendas();
    
    public void guardar (Agenda agenda);
    
    public void eliminar (Agenda agenda);
    
    public Agenda encontrarAgenda(Agenda agenda); 
    
    public List<Agenda> buscarAgendaPorEventoYAlumno(Long id_usuario, Long id_evento);
    
    public List<Agenda> buscarAgendasPorAlumno(Long id_usuario, Date fecha_evento);
    
    public List<Agenda> buscarAgendaPorEvento(Long id_evento);
    
    public List<Agenda> buscarAgendaSinUsar(Long id_usuario);

}
