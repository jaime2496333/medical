/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Inscripcion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InscripcionDao extends CrudRepository<Inscripcion, Long> {

    @Query("select ins from Inscripcion ins "
            + "join fetch ins.nivel ni "
            + "join fetch ins.usuario us "
            + "join fetch ins.curso cur "
            + "where cur.id_curso=?1 "
            + "and (ni.descripcion_nivel like %?2% or "
            + "us.username like %?2% or "
            + "us.nombres like %?2% or "
            + "us.apellidos like %?2%)")
    public List<Inscripcion> listarInscripcionActivasPorCursoNivel(Long id_curso,  String term);

    @Query("select i from Inscripcion i")
    public List<Inscripcion> listarInscripcionActivas();

    @Query("select insc from Inscripcion insc "
            + "join fetch insc.nivel ni "
            + "join fetch insc.usuario us "
            + "where us.id_usuario = ?1 and ni.id_nivel in (?2)")
    public List<Inscripcion> buscarInscripcionPorUsuarioYNivel(Long id_usuario, Long[] niveles);
    
    @Query("select insc from Inscripcion insc "
            + "join fetch insc.curso cur "
            + "join fetch insc.usuario us "
            + "where us.id_usuario = ?1 and cur.id_curso = ?2")
    public List<Inscripcion> buscarInscripcionPorUsuarioYCurso(Long id_usuario, Long id_curso);
    
    
    @Query("select ins from Inscripcion ins "
            + "join fetch ins.nivel ni "
            + "join fetch ins.usuario us "
            + "join fetch ins.curso cur "
            + "where (ni.descripcion_nivel like %?1% or "
            + "us.username like %?1% or "
            + "us.nombres like %?1% or "
            + "us.apellidos like %?1%) and ins.liberado=0")
    public List<Inscripcion> listarInscripcionesSinPagar(String term);
    
    
    @Query("select insc from Inscripcion insc "
            + "join fetch insc.nivel ni "
            + "join fetch insc.curso cur "
            + "join fetch insc.usuario us "
            + "where us.id_usuario = ?1 and insc.liberado=1 and insc.activo = 1")
    public List<Inscripcion> buscarInscripcionesPagadasPorAlumno(Long id_alumno);
    
    
    @Query("select insc from Inscripcion insc "
            + "join fetch insc.nivel ni "
            + "join fetch insc.curso cur "
            + "join fetch insc.usuario us "
            + "where us.id_usuario = ?1")
    public List<Inscripcion> buscarInscripcionesPorAlumno(Long id_alumno);
}
