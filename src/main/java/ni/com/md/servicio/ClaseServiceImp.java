/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.dao.ClaseDao;
import ni.com.md.domain.Clase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClaseServiceImp implements ClaseService {

    @Autowired
    private ClaseDao claseDao;

    @Override
    @Transactional(readOnly = true)
    public List<Clase> listarClases() {
        return (List<Clase>) claseDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Clase clase) {
        claseDao.save(clase);
    }

    @Override
    @Transactional
    public void eliminar(Clase clase) {
        claseDao.delete(clase);
    }

    @Override
    @Transactional(readOnly = true)
    public Clase encontrarClase(Clase clase) {
        return (Clase) claseDao.findById(clase.getId_clase()).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Clase> listarClasesPorNivel(Long id_nivel) {
        return claseDao.listarClasesPorNivel(id_nivel);
    }

}
