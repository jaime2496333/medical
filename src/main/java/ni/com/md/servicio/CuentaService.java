/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Cuenta;

/**
 *
 * @author julio
 */
public interface CuentaService {
    
    public List<Cuenta> listarCuentas();
    
    public void guardar (Cuenta cuenta);
    
    public void eliminar (Cuenta cuenta);
    
    public Cuenta encontrarCuenta(Cuenta cuenta); 
    
    public List<Cuenta> listarCuentasSinPagar(String term);
    
    public List<Cuenta> listarCuentasDeInscripcionSinPagar(String term);
    
    public List<Cuenta> listarCuentasPagadasPorCurso(Long id_curso);
}
