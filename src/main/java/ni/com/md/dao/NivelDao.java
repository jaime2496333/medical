/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;
import java.util.List;

import ni.com.md.domain.Nivel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface NivelDao extends CrudRepository<Nivel, Long> {
    
    @Query("select n from Nivel n join fetch n.curso c "
            + "where n.activo=1 and c.id_curso=?1")
    public List<Nivel> listarNivelesActivosPorCurso(Long id_curso);
    
    @Query("select n,c from Nivel n join fetch n.curso c where n.id_nivel=?1")
    public Nivel buscarNivelPorId(Long id_nivel) ;
    @Query("select n,c from Nivel n join fetch n.curso c where n.activo=1 order by c.id_curso,n.id_nivel")
    public List<Nivel> listarNivelesActivos();
    
    
}
