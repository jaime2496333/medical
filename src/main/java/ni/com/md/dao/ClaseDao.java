/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Clase;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface ClaseDao extends CrudRepository<Clase, Long>{
    
    @Query("select c,n from Clase c join fetch c.nivel n where n.id_nivel = ?1")
    public List<Clase> listarClasesPorNivel(Long id_nivel);
    
}
