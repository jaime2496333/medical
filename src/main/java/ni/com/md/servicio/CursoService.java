/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Curso;

/**
 *
 * @author julio
 */
public interface CursoService {
    
    public List<Curso> listarCursos();
    
    public void guardar (Curso curso);
    
    public void eliminar (Curso curso);
    
    public Curso encontrarCurso(Curso curso); 
    
    public Curso encontrarCursoPorId(Long id_curso); 
    
    public List<Curso> listarCursosActivos();
    
}
