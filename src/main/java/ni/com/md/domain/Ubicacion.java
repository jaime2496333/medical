package ni.com.md.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "ubicacion")
public class Ubicacion implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_ubicacion;

    private String nombre_ubicacion;
    private String calle;
    private String numero;
    private String colonia;
    private String ciudad;
    private String municipio;
    private String estado;
    private String ubicacion_gps;
    private String codigo_postal;
    private Boolean activo;
    private Double latitud;
    private Double longitud;
    private Long cantidad_modelos;
    private Long cupo_maximo;
    private String foto;

}
