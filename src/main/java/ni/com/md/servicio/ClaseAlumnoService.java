/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import ni.com.md.domain.ClaseAlumno;
import java.util.List;

/**
 *
 * @author julio
 */
public interface ClaseAlumnoService {

    public List<ClaseAlumno> listarClaseAlumnos();

    public void guardar(ClaseAlumno claseAlumno);

    public void eliminar(ClaseAlumno claseAlumno);

    public ClaseAlumno encontrarClaseAlumno(ClaseAlumno claseAlumno);
    
    public ClaseAlumno buscarPorAlumnoYCurso(Long id_alumno, Long id_Curso);
    
    public List<ClaseAlumno> buscarPorAlumno(Long id_alumno);

}
