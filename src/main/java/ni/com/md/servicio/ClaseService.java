/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Clase;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface ClaseService {
    
    public List<Clase> listarClases();
    
    public void guardar (Clase clase);
    
    public void eliminar (Clase clase);
    
    public Clase encontrarClase(Clase clase); 
    
    public List<Clase> listarClasesPorNivel(Long id_nivel);
}
