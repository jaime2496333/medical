/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.ComisionDao;
import ni.com.md.domain.Comision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ComisionServiceImp implements ComisionService {

    @Autowired
    private ComisionDao comisionDao;

    @Override
    @Transactional(readOnly = true)
    public List<Comision> listarComisiones() {
        return (List<Comision>) comisionDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Comision comision) {
        comisionDao.save(comision);
    }

    @Override
    @Transactional
    public void eliminar(Comision comision) {
        comisionDao.delete(comision);
    }

    @Override
    @Transactional(readOnly = true)
    public Comision encontrarAbono(Comision comision) {
        return comisionDao.findById(comision.getId_comision()).orElse(null);
    }

}
