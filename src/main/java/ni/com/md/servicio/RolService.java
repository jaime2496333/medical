/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;
import java.util.List;

import ni.com.md.domain.Rol;

public interface RolService {
     public List<Rol> listarRoles();
    
    public void guardar (Rol rol);
    
    public void eliminar (Rol rol);
    
    public Rol encontrarRol(Rol rol); 
}
