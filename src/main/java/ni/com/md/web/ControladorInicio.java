package ni.com.md.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import ni.com.md.domain.Abono;
import ni.com.md.domain.Agenda;
import ni.com.md.domain.Calendario;

import ni.com.md.domain.Categoria;
import ni.com.md.domain.Clase;
import ni.com.md.domain.ClaseAlumno;
import ni.com.md.domain.Comision;
import ni.com.md.domain.Cuenta;
import ni.com.md.domain.Curso;
import ni.com.md.domain.Division;
import ni.com.md.domain.Evento;
import ni.com.md.domain.Inscripcion;
import ni.com.md.domain.Material;
import ni.com.md.domain.MaterialCurso;
import ni.com.md.domain.Nivel;
import ni.com.md.domain.NivelCuenta;
import ni.com.md.domain.Persona;
import ni.com.md.domain.Rol;
import ni.com.md.domain.Tema;
import ni.com.md.domain.Ubicacion;
import ni.com.md.domain.Usuario;
import ni.com.md.servicio.AbonoService;
import ni.com.md.servicio.AgendaService;
import ni.com.md.servicio.CalendarioService;
import ni.com.md.servicio.CategoriaService;
import ni.com.md.servicio.ClaseAlumnoService;
import ni.com.md.servicio.ClaseService;
import ni.com.md.servicio.ComisionService;
import ni.com.md.servicio.CuentaService;
import ni.com.md.servicio.DivisionService;
import ni.com.md.servicio.CursoService;
import ni.com.md.servicio.EventoService;
import ni.com.md.servicio.InscripcionService;
import ni.com.md.servicio.MaterialCursoService;
import ni.com.md.servicio.MaterialService;
import ni.com.md.servicio.NivelCuentaService;
import ni.com.md.servicio.NivelService;
import ni.com.md.servicio.PersonaService;
import ni.com.md.servicio.RolService;
import ni.com.md.servicio.TemaService;
import ni.com.md.servicio.UbicacionService;
import ni.com.md.servicio.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@Slf4j
public class ControladorInicio {

    @Autowired
    private RolService rolService;
    @Autowired
    private PersonaService personaService;
    @Autowired
    private CategoriaService categoriaService;
    @Autowired
    private MaterialService materialService;
    @Autowired
    private DivisionService divisionService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private CursoService cursoService;
    @Autowired
    private UbicacionService ubicacionService;
    @Autowired
    private NivelService nivelService;
    @Autowired
    private TemaService temaService;
    @Autowired
    private MaterialCursoService materialCursoService;
    @Autowired
    private InscripcionService inscripcionService;
    @Autowired
    private AbonoService abonoService;
    @Autowired
    private ClaseService claseService;
    @Autowired
    private EventoService eventoService;
    @Autowired
    private AgendaService agendaService;
    @Autowired
    private CuentaService cuentaService;
    @Autowired
    private NivelCuentaService nivelCuentaService;
    @Autowired
    private CalendarioService calendarioService;
    @Autowired
    private ComisionService comisionService;
    @Autowired
    private ClaseAlumnoService claseAlumnoService;

    @GetMapping("/")
    public String inicio(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("index");
    }

    @GetMapping("/agregar")
    public String agregar(Persona persona) {
        return "modificar";
    }

    @PostMapping("/guardar")
    public String guardar(@Valid Persona persona, Errors errores) {
        if (errores.hasErrors()) {
            return "modificar";
        }
        personaService.guardar(persona);
        return "redirect:/";
    }

    @GetMapping("/editar/{idPersona}")
    public String editar(Persona persona, Model model, @AuthenticationPrincipal User user) {
        persona = personaService.encontrarPersona(persona);
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("persona", persona);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());

        return "modificar";
    }

    @GetMapping("/eliminar")
    public String eliminar(Persona persona) {
        personaService.eliminar(persona);
        return "redirect:/";
    }

    @GetMapping("/categorias")
    public String categorias(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var categorias = categoriaService.listarCategoriasActivas();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("categorias", categorias);

        return ("categorias");
    }

    @GetMapping("/materiales")
    public String materiales(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var materiales = materialService.listarMaterialesActivos();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("materiales", materiales);

        return ("materiales");
    }

    @GetMapping("/divisiones")
    public String divisiones(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var divisiones = divisionService.listarDivisionesActivas();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("divisiones", divisiones);

        return ("divisiones");
    }

    @GetMapping("/usuarios")
    public String mostrarUsuarios(Model model, @AuthenticationPrincipal User user) {
        String[] rolesDeUsuario = {"ROLE_PROFESOR", "ROLE_COORDINADOR"};
        var usuarios = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearUsuario");
        model.addAttribute("titulo", "Agregar Profesor / Coordinador");
        model.addAttribute("titulodelapagina", "Profesores");
        model.addAttribute("accion", "/usuariosbuscar");
        model.addAttribute("editar", "/editarUsuario");
        model.addAttribute("keyword", "");

        return ("usuarios");
    }

    @GetMapping("/usuariosbuscar")
    public String listarUsuariosPorUserName(Model model, String keyword, @AuthenticationPrincipal User user) {
        String[] rolesDeUsuario = {"ROLE_PROFESOR", "ROLE_COORDINADOR"};
        var usuarios = usuarioService.listarUsuarioPorUserName(rolesDeUsuario, keyword);
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearUsuario");
        model.addAttribute("titulo", "Agregar Profesor / Coordinador");
        model.addAttribute("titulodelapagina", "Profesores");
        model.addAttribute("accion", "/usuariosbuscar");
        model.addAttribute("editar", "/editarUsuario");
        model.addAttribute("keyword", keyword);

        return ("usuarios");
    }

    @GetMapping("/crearUsuario")
    public String crearUsuario(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("crearUsuario");
    }

    @PostMapping("/guardarUsuario")
    public String guardarUsuario(@Valid Usuario usuario,
            @RequestParam("file") MultipartFile imagen,
            @RequestParam(value = "chkProfesor", required = false) String chkProfesor,
            @RequestParam(value = "chkCoordinador", required = false) String chkCoordinador,
            @RequestParam(value = "chkAlumno", required = false) String chkAlumno,
            @RequestParam(value = "chkVendedor", required = false) String chkVendedor,
            @RequestParam(value = "url", required = false) String url,
            @RequestParam(value = "uri", required = false) String uri,
            Errors errores,
            Model model) {

        if (usuario.getId_usuario() == null) {
            Usuario usuarioDB = usuarioService.listarPropiedadesDeUsuario(usuario.getUsername());
            if (usuarioDB != null) {
                model.addAttribute("error", "Usuario ya existe");

                return uri;

            }

        }

        if (errores.hasErrors()) {
            return uri;
        }
        if (!imagen.isEmpty()) {
            Path directorioImagenes = Paths.get("src//main//resources//static/imagenes");
            String rutaAbsoluta = directorioImagenes.toFile().getAbsolutePath();
            try {
                byte[] bytesImg = imagen.getBytes();
                Path rutaCompleta = Paths.get(rutaAbsoluta + "//" + imagen.getOriginalFilename());
                Files.write(rutaCompleta, bytesImg);
                usuario.setfoto(imagen.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        }

        List<Rol> roles = new ArrayList<Rol>();
        if (chkProfesor != null) {
            Rol rol = new Rol();
            rol.setNombre("ROLE_PROFESOR");
            rolService.guardar(rol);
            roles.add(rol);

        }
        if (chkCoordinador != null) {
            Rol rol = new Rol();
            rol.setNombre("ROLE_COORDINADOR");
            rolService.guardar(rol);
            roles.add(rol);

        }
        if (chkAlumno != null) {
            Rol rol = new Rol();
            rol.setNombre("ROLE_ALUMNO");
            rolService.guardar(rol);
            roles.add(rol);

        }

        if (chkVendedor != null) {
            Rol rol = new Rol();
            rol.setNombre("ROLE_VENDEDOR");
            rolService.guardar(rol);
            roles.add(rol);

        }
        List<Inscripcion> inscripciones = new ArrayList<>();
        inscripciones = inscripcionService.buscarInscripcionesPorAlumno(usuario.getId_usuario());

        usuario.setRoles(roles);
        usuario.setInscripciones(inscripciones);

        usuarioService.guardarUsuario(usuario);
        return "redirect:" + url;
    }

    @GetMapping("/eliminarUsuario")
    public String eliminarUsuario(Usuario usuario) {
        usuarioService.eliminarUsuario(usuario);
        return "redirect:/usuarios";
    }

    @GetMapping("/editarUsuario")
    public String editarUsuario(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        usuario = usuarioService.buscarUsuario(usuario);

        Boolean esProfesor = false;
        Boolean esCoordinador = false;
        for (Rol rol : usuario.getRoles()) {
            if (rol.getNombre().equals("ROLE_PROFESOR")) {
                esProfesor = true;
            }
            if (rol.getNombre().equals("ROLE_COORDINADOR")) {
                esCoordinador = true;
            }
        }

        model.addAttribute("usuario", usuario);
        model.addAttribute("esprofesor", esProfesor);
        model.addAttribute("escoordinador", esCoordinador);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());

        return ("crearUsuario");
    }

    @GetMapping("/alumnos")
    public String Alumnos(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_ALUMNO"};
        var usuarios = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearAlumno");
        model.addAttribute("titulo", "Agregar Alumno");
        model.addAttribute("titulodelapagina", "Alumnos");
        model.addAttribute("accion", "/alumnosbuscar");
        model.addAttribute("editar", "/editarAlumno");
        model.addAttribute("keyword", "");

        return ("usuarios");
    }

    @GetMapping("/alumnosbuscar")
    public String AlumnosPorUserName(Model model, String keyword, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_ALUMNO"};
        var usuarios = usuarioService.listarUsuarioPorUserName(rolesDeUsuario, keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearAlumno");
        model.addAttribute("titulo", "Agregar Alumno");
        model.addAttribute("titulodelapagina", "Alumnos");
        model.addAttribute("accion", "/alumnosbuscar");
        model.addAttribute("editar", "/editarAlumno");
        model.addAttribute("keyword", keyword);

        return ("usuarios");
    }

    @GetMapping("/crearAlumno")
    public String crearAlumno(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("crearAlumno");
    }

    @GetMapping("/editarAlumno")
    public String editarAlumo(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        usuario = usuarioService.buscarUsuario(usuario);
        model.addAttribute("usuario", usuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("crearAlumno");
    }

    @GetMapping("/vendedores")
    public String Vendedores(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_VENDEDOR"};
        var usuarios = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearVendedor");
        model.addAttribute("titulo", "Agregar Vendedor");
        model.addAttribute("titulodelapagina", "Vendedores");
        model.addAttribute("accion", "/vendedoresbuscar");
        model.addAttribute("editar", "/editarVendedor");
        model.addAttribute("keyword", "");
        return ("usuarios");
    }

    @GetMapping("/vendedoresbuscar")
    public String vendedoresPorUserName(Model model, String keyword, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_VENDEDOR"};
        var usuarios = usuarioService.listarUsuarioPorUserName(rolesDeUsuario, keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearVendedor");
        model.addAttribute("titulo", "Agregar Vendedor");
        model.addAttribute("titulodelapagina", "Vendedores");
        model.addAttribute("accion", "/vendedoresbuscar");
        model.addAttribute("editar", "/editarVendedor");

        model.addAttribute("keyword", keyword);

        return ("usuarios");
    }

    @GetMapping("/crearVendedor")
    public String crearVendedor(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("crearVendedor");
    }

    @GetMapping("/editarVendedor")
    public String editarVendedor(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        usuario = usuarioService.buscarUsuario(usuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuario", usuario);
        return ("crearVendedor");
    }

    @GetMapping("/cursos")
    public String cursos(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var cursos = cursoService.listarCursosActivos();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("cursos", cursos);
        return ("cursos");
    }

    @GetMapping("/crearCurso")
    public String agregarCurso(Model model, Curso curso, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var categorias = categoriaService.listarCategoriasActivas();
        var divisiones = divisionService.listarDivisionesActivas();
        var materiales = materialService.listarMaterialesActivos();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("categorias", categorias);
        model.addAttribute("divisiones", divisiones);
        model.addAttribute("materiales", materiales);
        return ("crearCurso");
    }

    @GetMapping("/editarCurso")
    public String editarCurso(Curso curso, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        curso = cursoService.encontrarCurso(curso);
        var categorias = categoriaService.listarCategoriasActivas();
        var divisiones = divisionService.listarDivisionesActivas();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("divisiones", divisiones);
        model.addAttribute("categorias", categorias);
        model.addAttribute("curso", curso);
        return ("crearCurso");
    }

    @GetMapping("/configurarCurso")
    public String configurarCurso(Curso curso, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        curso = cursoService.encontrarCurso(curso);
        var categorias = categoriaService.listarCategoriasActivas();
        var divisiones = divisionService.listarDivisionesActivas();
        var materiales = materialService.listarMaterialesActivos();
        var niveles = nivelService.listarNivelesActivosPorCurso(curso.getId_curso());
        var temas = temaService.listarTemasActivosPorCurso(curso.getId_curso());
        var materialesCurso = materialCursoService.listarMaterialesPorCurso(curso.getId_curso());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("divisiones", divisiones);
        model.addAttribute("categorias", categorias);
        model.addAttribute("materiales", materiales);
        model.addAttribute("niveles", niveles);
        model.addAttribute("temas", temas);
        model.addAttribute("materialesCurso", materialesCurso);

        model.addAttribute("curso", curso);
        return ("configurarCurso");
    }

    @GetMapping("/agregarMaterialCurso")
    public String agregarMaterialCurso(MaterialCurso materialCurso,
            @RequestParam(value = "id_material", required = false) Long id_material,
            @RequestParam(value = "id_curso", required = false) Long id_curso) {

        Curso curso = cursoService.encontrarCursoPorId(id_curso);
        Material material = materialService.encontrarMaterialPorId(id_material);

        materialCurso.setCurso(curso);
        materialCurso.setMaterial(material);
        materialCursoService.guardar(materialCurso);
        return "redirect:/configurarCurso?id_curso=" + id_curso;
    }

    @GetMapping("/eliminarMaterialCurso")
    public String eliminarMaterialCurso(MaterialCurso materialCurso) {
        MaterialCurso materialCursoDB = materialCursoService.encontrarMaterialCurso(materialCurso);
        Curso cursoDB = cursoService.encontrarCurso(materialCursoDB.getCurso());

        materialCursoService.eliminar(materialCurso);
        return "redirect:/configurarCurso?id_curso=" + cursoDB.getId_curso().toString();
    }

    @PostMapping("/guardarCurso")
    public String guardarCurso(@Valid Curso curso,
            @RequestParam("file") MultipartFile imagen,
            Errors errores) {
        if (errores.hasErrors()) {
            return "cursos";
        }

        if (!imagen.isEmpty()) {
            Path directorioImagenes = Paths.get("src//main//resources//static/imagenes");
            String rutaAbsoluta = directorioImagenes.toFile().getAbsolutePath();
            try {
                byte[] bytesImg = imagen.getBytes();
                Path rutaCompleta = Paths.get(rutaAbsoluta + "//" + imagen.getOriginalFilename());
                Files.write(rutaCompleta, bytesImg);
                curso.setImagen_curso(imagen.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        }

        curso.setActivo(Boolean.TRUE);
        cursoService.guardar(curso);
        return "redirect:/cursos";
    }

    @GetMapping("/crearCategoria")
    public String agregarCategoria(Model model, Categoria categoria, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var categorias = categoriaService.listarCategorias();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("categorias", categorias);

        return ("crearCategoria");
    }

    @GetMapping("/editarCategoria")
    public String editarCategoria(Categoria categoria, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        categoria = categoriaService.encontrarCategoria(categoria);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("categoria", categoria);

        return ("crearCategoria");
    }

    @PostMapping("/guardarCategoria")
    public String guardarCategoria(@Valid Categoria categoria, Errors errores) {
        if (errores.hasErrors()) {
            return "categorias";
        }
        categoriaService.guardar(categoria);
        return "redirect:/categorias";
    }

    @GetMapping("/eliminarCategoria")
    public String eliminarCategoria(Categoria categoria) {
        categoriaService.eliminar(categoria);
        return "redirect:/categorias";
    }

    @GetMapping("/crearMaterial")
    public String agregarMaterial(Model model, Material material, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var divisiones = divisionService.listarDivisionesActivas();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("divisiones", divisiones);
        return ("crearMaterial");
    }

    @GetMapping("/editarMaterial")
    public String editarMaterial(Material material, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var divisiones = divisionService.listarDivisionesActivas();
        material = materialService.encontrarMaterial(material);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("material", material);
        model.addAttribute("divisiones", divisiones);

        return ("crearMaterial");
    }

    @PostMapping("/guardarMaterial")
    public String guardarMaterial(@Valid Material material, Errors errores) {
        if (errores.hasErrors()) {
            return "materiales";
        }
        materialService.guardar(material);
        return "redirect:/materiales";
    }

    @GetMapping("/eliminarMaterial")
    public String eliminarMaterial(Material material) {
        materialService.eliminar(material);
        return "redirect:/materiales";
    }

    @GetMapping("/crearDivision")
    public String agregarDivision(Model model, Division division, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var divisiones = divisionService.listarDivisiones();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("divisiones", divisiones);
        return ("crearDivision");
    }

    @GetMapping("/editarDivision")
    public String editaDivision(Division division, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        division = divisionService.encontrarDivision(division);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("division", division);

        return ("crearDivision");
    }

    @PostMapping("/guardarDivision")
    public String guardarDivision(@Valid Division division, Errors errores) {
        if (errores.hasErrors()) {
            return "divisiones";
        }
        divisionService.guardar(division);
        return "redirect:/divisiones";
    }

    @GetMapping("/eliminarDivision")
    public String eliminarDivision(Division division) {
        divisionService.eliminar(division);
        return "redirect:/divisiones";
    }

    @GetMapping("/eliminarCurso")
    public String eliminarCurso(Curso curso) {
        cursoService.eliminar(curso);
        return "redirect:/cursos";
    }

    @GetMapping("/ubicaciones")
    public String ubicaciones(Model model, @AuthenticationPrincipal User user, @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var ubicaciones = ubicacionService.listarUbicacionesActivas(keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("ubicaciones", ubicaciones);
        model.addAttribute("keyword", keyword);

        return ("ubicaciones");
    }

    @GetMapping("/crearUbicacion")
    public String agregarUbicacion(Model model, Ubicacion ubicacion, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var ubicaciones = ubicacionService.listarUbicaciones();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("ubicaciones", ubicaciones);

        return ("crearUbicacion");

    }

    @GetMapping("/editarUbicacion")
    public String editarUbicacion(Ubicacion ubicacion, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        ubicacion = ubicacionService.encontrarUbicacion(ubicacion);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("ubicacion", ubicacion);

        return ("crearUbicacion");
    }

    @PostMapping("/guardarUbicacion")
    public String guardarUbicacion(@Valid Ubicacion ubicacion, Errors errores, @RequestParam("imagen") MultipartFile imagen) {
        if (errores.hasErrors()) {
            return "ubicaciones";
        }

        if (!imagen.isEmpty()) {
            Path directorioImagenes = Paths.get("src//main//resources//static/imagenes");
            String rutaAbsoluta = directorioImagenes.toFile().getAbsolutePath();
            try {
                byte[] bytesImg = imagen.getBytes();
                Path rutaCompleta = Paths.get(rutaAbsoluta + "//" + imagen.getOriginalFilename());
                Files.write(rutaCompleta, bytesImg);
                ubicacion.setFoto(imagen.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        } else {
            if (ubicacion.getId_ubicacion() != null) {
                Ubicacion UbicacionDB = ubicacionService.encontrarUbicacion(ubicacion);
                ubicacion.setFoto(UbicacionDB.getFoto());
            }
        }
        ubicacion.setActivo(Boolean.TRUE);
        ubicacionService.guardar(ubicacion);
        return "redirect:/ubicaciones";
    }

    @GetMapping("/eliminarUbicacion")
    public String eliminarUbicacion(Ubicacion ubicacion) {
        ubicacionService.eliminar(ubicacion);
        return "redirect:/ubicaciones";
    }

    @GetMapping("/crearNivel")
    public String agregarNivel(Model model, Nivel nivel, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var niveles = nivelService.listarNiveles();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("niveles", niveles);

        return ("crearNivel");
    }

    @GetMapping("/editarNivel")
    public String editarNivel(Nivel nivel, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        nivel = nivelService.encontrarNivel(nivel);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("nivel", nivel);

        return ("crearNivel");
    }

    @PostMapping("/guardarNivel")
    public String guardarNivel(@Valid Nivel nivel,
            @RequestParam("id_curso") Long id_curso,
            Errors errores) {
        if (errores.hasErrors()) {
            return "niveles";
        }
        nivelService.guardarNivel(nivel, id_curso);
        return "redirect:/configurarCurso?id_curso=" + id_curso;
    }

    @GetMapping("/eliminarNivel")
    public String eliminarninvel(Nivel nivel) {
        Nivel nivelDB = nivelService.encontrarNivel(nivel);
        String id_curso = nivelDB.getCurso().getId_curso().toString();
        nivelService.eliminar(nivelDB);
        return "redirect:/configurarCurso?id_curso=" + id_curso;
    }

    @PostMapping("/guardarTema")
    public String guardartema(@Valid Tema tema,
            @RequestParam("id_curso") Long id_curso,
            Errors errores) {
        if (errores.hasErrors()) {
            return "niveles";
        }
        temaService.guardar(tema);
        return "redirect:/configurarCurso?id_curso=" + id_curso;
    }

    @GetMapping("/editarTema")
    public String editarTema(Tema tema, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        tema = temaService.encontrarTema(tema);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("tema", tema);

        return ("redirect:/guardarTema");
    }

    @GetMapping("/eliminarTema")
    public String eliminarTema(Tema tema) {
        Tema temaDB = temaService.encontrarTema(tema);
        String id_nivel = temaDB.getNivel().getId_nivel().toString();
        String id_curso = temaDB.getNivel().getCurso().getId_curso().toString();
        //String id_curso = temaDB.getCurso().getId_curso().toString();
        temaService.eliminar(temaDB);

        return "redirect:/configurarCurso?id_curso=" + id_curso;
    }

    @GetMapping("/verMapa")
    public String verMapa(Ubicacion ubicacion) {

        return "redirect:/ubicaciones";
    }

    @GetMapping("/inscripciones")
    public String inscripciones(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        String[] rolesDeUsuario = {"ROLE_PROFESOR", "ROLE_COORDINADOR"};
        var usuarios = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearUsuario");
        model.addAttribute("titulo", "Inscribir Alumno");
        model.addAttribute("titulodelapagina", "Alumnos Inscritos");
        model.addAttribute("accion", "/usuariosbuscar");
        model.addAttribute("editar", "/editarUsuario");
        model.addAttribute("keyword", "");
        var cursos = cursoService.listarCursosActivos();
        model.addAttribute("cursos", cursos);

        return ("inscripciones");
    }

    @GetMapping("/crearInscripcion")
    public String crearInscripcion(Curso curso, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        curso = cursoService.encontrarCurso(curso);
        String[] rolesDeUsuario = {"ROLE_ALUMNO"};
        //var alumnos = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        var niveles = nivelService.listarNivelesActivosPorCurso(curso.getId_curso());
        var temas = temaService.listarTemasActivosPorCurso(curso.getId_curso());
        var cuentas = cuentaService.listarCuentasPagadasPorCurso(curso.getId_curso());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("niveles", niveles);
        model.addAttribute("temas", temas);
        model.addAttribute("cuentas", cuentas);
        model.addAttribute("curso", curso);
        model.addAttribute("mensaje", null);

        return ("crearInscripcion");
    }

    @PostMapping("/guardarInscripcion")
    public String guardarInscripcion(Inscripcion inscripcion, Curso curso,
            @RequestParam(value = "listAlumno", required = false) String username,
            @RequestParam(value = "chkCursoCompleto", required = false, defaultValue = "false") Boolean cursoCompleto,
            @RequestParam(value = "niveles[]", required = false) Long[] listaNiveles,
            @RequestParam(value = "modalidadDePago", required = false) String modalidadDePago,
            Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        curso = cursoService.encontrarCurso(curso);
        var usuario = usuarioService.listarPropiedadesDeUsuario(username);
        List<Inscripcion> inscripcionesABuscar = inscripcionService.buscarInscripcionPorUsuarioYNivel(usuario.getId_usuario(), listaNiveles);
        Boolean mensaje = Boolean.FALSE;
        if (!inscripcionesABuscar.isEmpty()) {
            String[] rolesDeUsuario = {"ROLE_ALUMNO"};
            var alumnos = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
            var niveles = nivelService.listarNivelesActivosPorCurso(curso.getId_curso());
            var temas = temaService.listarTemasActivosPorCurso(curso.getId_curso());

            model.addAttribute("usuarioImagen", usuariologeado.getfoto());
            model.addAttribute("niveles", niveles);
            model.addAttribute("temas", temas);
            model.addAttribute("alumnos", alumnos);
            model.addAttribute("curso", curso);
            model.addAttribute("mensaje", "Alumno se encuentra inscrito");
            return ("crearInscripcion");
        }

        Cuenta cuenta = new Cuenta();
        List<NivelCuenta> nivelesCuenta = new ArrayList<>();
        cuenta.setAlumno(usuario);
        float totalDeLaCuenta = 0;

        Date fechaLimiteDePago = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaLimiteDePago);
        cal.add(Calendar.DAY_OF_MONTH, 10);
        fechaLimiteDePago = cal.getTime();

        Date fechaVigencia = new Date();
        cal.setTime(fechaVigencia);
        cal.add(Calendar.MONTH, 6);
        fechaVigencia = cal.getTime();

        for (Long idNivel : listaNiveles) {
            inscripcion = new Inscripcion();
            var nivel = nivelService.buscarNivelPorId(idNivel);
            inscripcion.setNivel(nivel);
            inscripcion.setCurso(nivel.getCurso());
            inscripcion.setActivo(Boolean.TRUE);
            inscripcion.setLiberado(Boolean.FALSE);
            inscripcion.setPrecio(cursoCompleto == true ? nivel.getPrecio_curso_completo() : nivel.getPrecio_unitario());
            inscripcion.setUsuario(usuario);
            inscripcion.setModalidad_pago(modalidadDePago);
            inscripcionService.guardar(inscripcion);
            mensaje = Boolean.TRUE;

            totalDeLaCuenta += inscripcion.getPrecio();
            NivelCuenta nivelCuenta = new NivelCuenta();
            nivelCuenta.setLiberado(Boolean.FALSE);
            nivelCuenta.setNivel(nivel);
            nivelCuenta.setInscripcion(inscripcion);
            nivelCuentaService.guardar(nivelCuenta);
            nivelesCuenta.add(nivelCuenta);

        }

        cuenta.setMonto_cuenta(totalDeLaCuenta);
        cuenta.setAbonado(0);
        cuenta.setLiberado(Boolean.FALSE);
        cuenta.setUsuarioCreador(usuariologeado);
        cuenta.setNivelesCuentas(nivelesCuenta);
        cuenta.setVigencia(fechaVigencia);
        cuenta.setFecha_limite_de_pago(fechaLimiteDePago);
        cuentaService.guardar(cuenta);

        return ("redirect:/verAlumnosInscritos?id_curso=" + curso.getId_curso());
    }

    //Alumnos Inscritos
    @GetMapping("/alumnosInscritos")
    public String AlumnosInscritos(Model model,
            @AuthenticationPrincipal User user,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword) {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var cuentas = cuentaService.listarCuentasSinPagar(keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("cuentas", cuentas);
        model.addAttribute("titulodelapagina", "Alumnos Inscritos");
        model.addAttribute("keyword", keyword);
        model.addAttribute("id_usuario_logueado", usuariologeado.getId_usuario());

        return ("alumnosInscritos");
    }

    @GetMapping("/alumnosInscritosBuscar")
    public String AlumnosPorUserNameInscritos(Model model, String keyword, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        String[] rolesDeUsuario = {"ROLE_ALUMNO"};
        var alumnos = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        var usuarios = usuarioService.listarUsuarioPorUserName(rolesDeUsuario, keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuarios", usuarios);
        model.addAttribute("liga", "crearAlumno");
        model.addAttribute("titulo", "Agregar Alumno");
        model.addAttribute("titulodelapagina", "Alumnos");
        model.addAttribute("accion", "/alumnosbuscar");
        model.addAttribute("editar", "/editarAlumno");
        model.addAttribute("keyword", keyword);

        return ("alumnosInscritos");
    }

    @GetMapping("/abonarACursoNivel")
    public String abonarACursoNivel(Usuario usuario, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        usuario = usuarioService.buscarUsuario(usuario);
        model.addAttribute("usuario", usuario);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        return ("abonarACursoNivel");
    }

    @GetMapping("/verAlumnosInscritos")
    public String verAlumnosInscritos(Inscripcion inscripcion,
            Model model,
            @RequestParam(value = "id_curso", required = false) Long id_curso,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var curso = cursoService.encontrarCursoPorId(id_curso);
        var usuarios = usuarioService.listarUsuariosPorCursoYNivel(id_curso, keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());

        model.addAttribute("usuarios", usuarios);
        model.addAttribute("curso", curso);
        model.addAttribute("keyword", keyword);
        model.addAttribute("id_curso", id_curso);

        return ("verAlumnosInscritos");
    }

    @PostMapping("/guardarAbono")
    public String guardarAbono(Abono abono,
            Model model) {
        abonoService.guardar(abono);
        Cuenta cuentaDB = cuentaService.encontrarCuenta(abono.getCuenta());
        cuentaDB.setAbonado(cuentaDB.getAbonado() + abono.getMonto_del_abono());
        if (cuentaDB.getMonto_cuenta() == cuentaDB.getAbonado()) {
            cuentaDB.setLiberado(Boolean.TRUE);
        }
        cuentaService.guardar(cuentaDB);

        return ("redirect:/alumnosInscritos");
    }

    @PostMapping("/guardarAbonoInscripcion")
    public String guardarAbonoInscripcion(Abono abono,
            Model model,
            @AuthenticationPrincipal User user) {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        abono.setUsuarioqueRecibeabono(usuariologeado);
        abonoService.guardar(abono);

        Cuenta cuentaDB = cuentaService.encontrarCuenta(abono.getCuenta());
        cuentaDB.setAbonado(cuentaDB.getAbonado() + abono.getMonto_del_abono());

        if (cuentaDB.getMonto_cuenta() == cuentaDB.getAbonado()) {
            cuentaDB.setLiberado(Boolean.TRUE);
            //Hasta despues de haber sido saldada la cuanta de inscripcion se graba una comision para el vendedor;
            Comision comisionParaVendedor = new Comision();
            comisionParaVendedor.setUsuario(cuentaDB.getVendedor());
            comisionParaVendedor.setAlumno(cuentaDB.getAlumno());
            comisionParaVendedor.setMonto_comision(cuentaDB.getCurso().getComision_vendedor());
            comisionParaVendedor.setCurso(cuentaDB.getCurso());

            comisionService.guardar(comisionParaVendedor);
            //Se incrementa el saldo total de coisiones del vendedor
            Usuario vendedor = usuarioService.buscarUsuario(cuentaDB.getVendedor());
            vendedor.setMonto_comision_acumulada(vendedor.getMonto_comision_acumulada() + cuentaDB.getCurso().getComision_vendedor());
            usuarioService.guardarUsuario(vendedor);
        }
        cuentaService.guardar(cuentaDB);

        return ("redirect:/pagoInscripcion");
    }

    @GetMapping("/crearClase")
    public String crearClase(Curso curso, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());

        var niveles = nivelService.listarNivelesActivos();
        model.addAttribute("niveles", niveles);

        return ("crearClase");
    }

    @PostMapping("/guardarClase")
    public String guardarClase(Clase clase) {
        claseService.guardar(clase);

        return ("redirect:/crearClase");
    }

    @GetMapping("/verClasesPorCursoYNivel")
    public String verClasesPorCursoYNivel(Clase clase,
            Model model, @AuthenticationPrincipal User user,
            @RequestParam(value = "id_nivel", required = false) Long id_nivel
    ) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var clases = claseService.listarClasesPorNivel(id_nivel);
        var nivel = nivelService.buscarNivelPorId(id_nivel);
        var curso = cursoService.encontrarCursoPorId(nivel.getCurso().getId_curso());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("clases", clases);
        model.addAttribute("nivel", nivel);
        model.addAttribute("curso", curso);
        return ("verClasesPorCursoYNivel");
    }

    @GetMapping("/eliminarClase")
    public String eliminarClase(Clase clase) {
        Clase claseDB = claseService.encontrarClase(clase);
        String id_nivel = claseDB.getNivel().getId_nivel().toString();
        claseService.eliminar(clase);
        return "redirect:/verClasesPorCursoYNivel?id_nivel=" + id_nivel;
    }

    @GetMapping("/crearEvento")
    public String crearEvento(Evento evento,
            @RequestParam(value = "ubicacion", required = false, defaultValue = "") Ubicacion ubicacion,
            Model model,
            @AuthenticationPrincipal User user,
            @RequestParam(value = "inicio", required = false, defaultValue = "") String inicio,
            @RequestParam(value = "fin", required = false, defaultValue = "") String fin) throws ParseException {

        Date fechaInicio, fechaFin;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();

        fechaInicio = inicio.equals("") ? new Date() : (Date) formatter.parse(inicio);

        if (fin.equals("")) {
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, 1);
            fechaFin = cal.getTime();

        } else {
            fechaFin = (Date) formatter.parse(fin);
        }

        Usuario usuariologeado;

        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_PROFESOR"};
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());

        var cursos = cursoService.listarCursosActivos();
        var ubicaciones = ubicacionService.listarUbicacionesActivas("");
        var eventos = eventoService.listarEventosPorFecha(fechaInicio,
                fechaFin,
                ubicacion == null ? ubicaciones.get(0).getId_ubicacion() : ubicacion.getId_ubicacion());
        var maestros = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);
        model.addAttribute("eventos", eventos);
        model.addAttribute("cursos", cursos);
        model.addAttribute("ubicaciones", ubicaciones);
        model.addAttribute("id_usuario_logueado", usuariologeado.getId_usuario());
        model.addAttribute("maestros", maestros);

        model.addAttribute("ubicacion", ubicacion != null ? ubicacion : ubicaciones.get(0));
        model.addAttribute("inicio", inicio);
        model.addAttribute("fin", fin);

        return ("crearEvento");
    }

    @GetMapping("/BandejaMaestro")
    public String BandejaMaestro(Evento evento,
            @RequestParam(value = "ubicacion", required = false, defaultValue = "") Ubicacion ubicacion,
            Model model,
            @AuthenticationPrincipal User user,
            @RequestParam(value = "inicio", required = false, defaultValue = "") String inicio,
            @RequestParam(value = "fin", required = false, defaultValue = "") String fin) throws ParseException {

        Date fechaInicio, fechaFin;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        
        Calendar aux = Calendar.getInstance();
        aux.setTime(new Date());
        aux.add(Calendar.DAY_OF_MONTH, -1);

        fechaInicio = inicio.equals("") ? aux.getTime() : (Date) formatter.parse(inicio);

        if (fin.equals("")) {
            cal.setTime(new Date());
            cal.add(Calendar.MONTH, 1);
            fechaFin = cal.getTime();

        } else {
            fechaFin = (Date) formatter.parse(fin);
        }

        Usuario usuariologeado;

        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        var eventos = eventoService.listarEventosPorProfesor(fechaInicio,
                fechaFin, usuariologeado.getId_usuario());

        model.addAttribute("eventos", eventos);
        model.addAttribute("id_usuario_logueado", usuariologeado.getId_usuario());
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("inicio", inicio);
        model.addAttribute("fin", fin);

        return ("BandejaMaestro");
    }

    @PostMapping("/guardarEvento")
    public String guardarEvento(Evento evento,
            @RequestParam(value = "fecha", required = false) String fecha,
            @RequestParam(value = "publicado", required = false, defaultValue = "false") Boolean publicado) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = (Date) formatter.parse(fecha);
        evento.setPublicado(publicado);
        evento.setFecha_hora(date);
        eventoService.guardar(evento);

        return ("redirect:/crearEvento?ubicacion=" + evento.getUbicacion().getId_ubicacion());
    }

    @GetMapping("/eliminarEvento")
    public String eliminarEvento(Evento evento) {
        eventoService.eliminar(evento);
        return "redirect:/crearEvento";
    }

    @GetMapping("/publicarEvento")
    public String publicarEvento(Evento evento) {
        Evento eventoDB = eventoService.encontrarEvento(evento);
        eventoDB.setPublicado(!eventoDB.getPublicado());
        eventoService.guardar(eventoDB);
        return "redirect:/crearEvento?ubicacion=" + eventoDB.getUbicacion().getId_ubicacion();
    }

    @GetMapping("/agendar")
    public String agendar(Agenda agenda,
            Model model,
            @AuthenticationPrincipal User user,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Date fechaInicio = new Date();
        Date fechaFin = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaFin);
        cal.add(Calendar.DAY_OF_MONTH, 7);
        fechaFin = cal.getTime();

        List<Inscripcion> inscripcionesPagadasDelAlumno = inscripcionService.buscarInscripcionesPagadasPorAlumno(usuariologeado.getId_usuario());
        Long[] cursosDelAlumno = new Long[inscripcionesPagadasDelAlumno.size()];
        int i = 0;
        for (Inscripcion inscripcion : inscripcionesPagadasDelAlumno) {
            cursosDelAlumno[i++] = inscripcion.getCurso().getId_curso();
        }
        var eventos = eventoService.listarEventosPorCursoPagadoDelAlumno(cursosDelAlumno, fechaInicio, fechaFin, keyword);
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("eventos", eventos);
        model.addAttribute("idUsuario", usuariologeado.getId_usuario());
        model.addAttribute("mensaje", null);
        model.addAttribute("keyword", keyword);

        return "agendar";

    }

    @GetMapping("/AsistenciaAgendar")
    public String AsistenciaAgendar(Agenda agenda, Model model, @AuthenticationPrincipal User user) {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Agenda agendaDB = agendaService.encontrarAgenda(agenda);
        Evento eventoDB = eventoService.encontrarEvento(agendaDB.getEvento());
        Ubicacion ubicacionDB = ubicacionService.encontrarUbicacion(eventoDB.getUbicacion());

        Curso curso = eventoDB.getCursos().get(0);

        ClaseAlumno claseAlumno = claseAlumnoService.buscarPorAlumnoYCurso(agendaDB.getAlumno().getId_usuario(), curso.getId_curso());
        if (claseAlumno == null) {
            List<Inscripcion> inscripciones = inscripcionService.buscarInscripcionesPagadasPorAlumno(agendaDB.getAlumno().getId_usuario());
            int numeroDeClases = 0;
            for (Inscripcion insPagada : inscripciones) {
                if (insPagada.getCurso() == eventoDB.getCursos().get(0)) {
                    numeroDeClases += temaService.listarTemasActivosPorCurso(insPagada.getCurso().getId_curso()).size();
                }
            }

            claseAlumno.setAlumno(agendaDB.getAlumno());
            claseAlumno.setCurso(curso);
            claseAlumno.setClases_tomadas(0);
            claseAlumno.setNumero_clases(numeroDeClases);
        }
//        if (!agendaDB.getAsistencia()) {
//            claseAlumno.setClases_tomadas(claseAlumno.getClases_tomadas() + 1);
//        }
        claseAlumno.setFecha_proxima_clase(new Date());

        claseAlumnoService.guardar(claseAlumno);

        agendaDB.setAsistencia(Boolean.TRUE);
        agendaDB.setFecha_asistencia(new Date());
        agendaDB.setProfesor(usuariologeado);
        agendaDB.setUbicacion(ubicacionDB);

        agendaService.guardar(agendaDB);

        if (claseAlumno.getClases_tomadas() == claseAlumno.getNumero_clases()) {
            //Aqui cerramos las inscripciones
            List<Inscripcion> inscripcionesPagadas = inscripcionService.buscarInscripcionPorUsuarioYCurso(agendaDB.getAlumno().getId_usuario(),
                    curso.getId_curso());

            for (Inscripcion inscripcion : inscripcionesPagadas) {
                inscripcion.setActivo(Boolean.FALSE);
                inscripcionService.guardar(inscripcion);
            }

            //Eliminamos las agendas no utilizadas para liberar espacio
            List<Agenda> agendasSinUsar = agendaService.buscarAgendaSinUsar(agendaDB.getAlumno().getId_usuario());
            for (Agenda agendaSinUsar : agendasSinUsar) {

                Evento eventoAActualizar = eventoService.encontrarEvento(agenda.getEvento());
                eventoAActualizar.setAlumnos_inscritos(eventoAActualizar.getAlumnos_inscritos() - 1);
                eventoService.guardar(eventoAActualizar);
                agendaService.eliminar(agendaSinUsar);
            }
        }

        return ("redirect:/controlDeAsistencia?id_evento=" + eventoDB.getId_evento());

    }

    @GetMapping("/NoAsistenciaAgendar")
    public String NoAsistenciaAgendar(Agenda agenda, Model model, @AuthenticationPrincipal User user) {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Agenda agendaDB = agendaService.encontrarAgenda(agenda);
        Evento eventoDB = eventoService.encontrarEvento(agendaDB.getEvento());
        Ubicacion ubicacionDB = ubicacionService.encontrarUbicacion(eventoDB.getUbicacion());

        Curso curso = eventoDB.getCursos().get(0);

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 7); //Se penaliza al alumno con 7 dias

        ClaseAlumno claseAlumno = claseAlumnoService.buscarPorAlumnoYCurso(agendaDB.getAlumno().getId_usuario(), curso.getId_curso());
        if (claseAlumno == null) {
            List<Inscripcion> inscripciones = inscripcionService.buscarInscripcionesPagadasPorAlumno(agendaDB.getAlumno().getId_usuario());
            int numeroDeClases = 0;
            for (Inscripcion insPagada : inscripciones) {
                if (insPagada.getCurso() == eventoDB.getCursos().get(0)) {
                    numeroDeClases += temaService.listarTemasActivosPorCurso(insPagada.getCurso().getId_curso()).size();
                }
            }

            claseAlumno.setAlumno(agendaDB.getAlumno());
            claseAlumno.setCurso(curso);
            claseAlumno.setClases_tomadas(0);
            claseAlumno.setNumero_clases(numeroDeClases);
        }

//        if (agendaDB.getAsistencia()) {
//            claseAlumno.setClases_tomadas(claseAlumno.getClases_tomadas() - 1);
//        }
        claseAlumno.setFecha_proxima_clase(calendar.getTime());

        claseAlumnoService.guardar(claseAlumno);

        agendaDB.setAsistencia(Boolean.FALSE);
        agendaDB.setFecha_asistencia(new Date());
        agendaDB.setProfesor(usuariologeado);
        agendaDB.setUbicacion(ubicacionDB);

        agendaService.guardar(agendaDB);

        return ("redirect:/controlDeAsistencia?id_evento=" + eventoDB.getId_evento());

    }

    @GetMapping("/guardarAgenda")
    public String guardarAgenda(Agenda agenda,
            @RequestParam(value = "id_evento", required = false) Long id_evento,
            Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Evento eventoDB = eventoService.buscarPorId(id_evento);
        List<Agenda> miAgenda = agendaService.buscarAgendaPorEventoYAlumno(usuariologeado.getId_usuario(), id_evento);
        Boolean esGuardado = Boolean.FALSE;
        String mensaje = "";
        if (miAgenda.isEmpty()) {

            if (eventoService.comprobarCupoEvento(eventoDB)) {
                agenda.setEvento(eventoDB);
                agenda.setAlumno(usuariologeado);
                agendaService.guardar(agenda);

                esGuardado = Boolean.TRUE;

            } else {
                esGuardado = Boolean.FALSE;
                mensaje = "Cupo Lleno";

            }
        } else {
            esGuardado = Boolean.FALSE;
            mensaje = "Ya reservaste en este Evento";

        }
        if (!esGuardado) {
            List<Inscripcion> inscripcionesPagadasDelAlumno = inscripcionService.buscarInscripcionesPagadasPorAlumno(usuariologeado.getId_usuario());
            Long[] cursosDelAlumno = new Long[inscripcionesPagadasDelAlumno.size()];
            int i = 0;
            for (Inscripcion inscripcion : inscripcionesPagadasDelAlumno) {
                cursosDelAlumno[i++] = inscripcion.getCurso().getId_curso();
            }
            Date fechaInicio = new Date();
            Date fechaFin = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(fechaFin);
            cal.add(Calendar.DAY_OF_MONTH, 7);
            fechaFin = cal.getTime();
            var eventos = eventoService.listarEventosPorCursoPagadoDelAlumno(cursosDelAlumno, fechaInicio, fechaFin, "");
            model.addAttribute("usuarioImagen", usuariologeado.getfoto());
            model.addAttribute("eventos", eventos);
            model.addAttribute("idUsuario", usuariologeado.getId_usuario());
            model.addAttribute("mensaje", mensaje);
            return "agendar";
        }
        return "redirect:/agendas";

    }

    @GetMapping("/agendas")
    public String miAgenda(Agenda agenda, Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Date fechaActual = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaActual);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        fechaActual = cal.getTime();
        

        List<Agenda> agendas = agendaService.buscarAgendasPorAlumno(usuariologeado.getId_usuario(), new Date());
        List<ClaseAlumno> claseAlumnos = claseAlumnoService.buscarPorAlumno(usuariologeado.getId_usuario());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("agendas", agendas);
        model.addAttribute("fechaActual", fechaActual);
        model.addAttribute("claseAlumnos", claseAlumnos);

        return "agendas";
    }

    @GetMapping("/eliminarAgenda")
    public String eliminarAgenda(Agenda agenda, @RequestParam(value = "id_agenda", required = false) Long id_agenda, @AuthenticationPrincipal User user) {
        Agenda agendaDB = agendaService.encontrarAgenda(agenda);

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

     
        agendaService.eliminar(agendaDB);
        Evento eventoDB = eventoService.buscarPorId(agendaDB.getEvento().getId_evento());
        int cuantosAlumnosVan = eventoDB.getAlumnos_inscritos();
        eventoDB.setAlumnos_inscritos(cuantosAlumnosVan - 1);
        eventoService.guardar(eventoDB);
        return "redirect:/agendas";
    }

    @PostMapping("/asignarMaestro")
    public String asignarMaestro(Evento evento, @RequestParam(value = "maestroEvento", required = false) String username) {
        Evento eventoDB = eventoService.encontrarEvento(evento);
        Usuario maestro = usuarioService.listarPropiedadesDeUsuario(username);
        eventoDB.setMaestro(maestro);
        eventoService.guardar(eventoDB);

        return ("redirect:/crearEvento?ubicacion=" + eventoDB.getUbicacion().getId_ubicacion());
    }

    @GetMapping("/crearCalendario")
    public String crearCalendario(Model model, Ubicacion ubicacion, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        //ubicacion = ubicacionService.encontrarUbicacion(ubicacion);
        var calendarios = calendarioService.listarEventosPorUbicacion(ubicacion.getId_ubicacion());
        var cursos = cursoService.listarCursosActivos();
        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("calendarios", calendarios);
        model.addAttribute("ubicacion", ubicacion);
        model.addAttribute("cursos", cursos);

        return ("crearCalendario");

    }

    @PostMapping("/guardarCalendario")
    public String guardarCalendario(Calendario calendario,
            @RequestParam(value = "fecha_de_inicio", required = false) String fecha_inicio,
            @RequestParam(value = "publicado", required = false, defaultValue = "false") Boolean publicado,
            @RequestParam(value = "fecha_de_fin", required = false) String fecha_fin,
            @AuthenticationPrincipal User user) throws ParseException {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = (Date) formatter.parse(fecha_inicio);
        calendario.setFecha_inicio(date);
        date = (Date) formatter.parse(fecha_fin);
        calendario.setFecha_fin(date);
        calendario.setPublicado(publicado);
        calendario.setActivo(Boolean.TRUE);

        calendarioService.guardar(calendario, usuariologeado);

        return ("redirect:/crearCalendario?ubicacion=" + calendario.getUbicacion().getId_ubicacion());
    }

    @GetMapping("/eliminarCalendario")
    public String eliminarCalendario(Calendario calendario, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        Calendario calendarioDb = calendarioService.encontrarCalendario(calendario);
        Long idUbicacion = calendarioDb.getUbicacion().getId_ubicacion();
        calendarioDb.setActivo(Boolean.FALSE);
        calendarioDb.setPublicado(Boolean.FALSE);
        calendarioService.guardar(calendarioDb, usuariologeado);
        return "redirect:/crearCalendario?ubicacion=" + idUbicacion;
    }

    @GetMapping("/verMaestroYAlumnosDelEvento")
    public String verMaestroYAlumnosDelEvento(Model model, Evento evento, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var eventoDB = eventoService.encontrarEvento(evento);
        var agendas = agendaService.buscarAgendaPorEvento(evento.getId_evento());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("evento", eventoDB);
        model.addAttribute("agendas", agendas);
        return "verMaestroYAlumnosDelEvento";
    }

    @GetMapping("/controlDeAsistencia")
    public String controlDeAsistencia(Model model, Evento evento, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        var eventoDB = eventoService.encontrarEvento(evento);
        var agendas = agendaService.buscarAgendaPorEvento(evento.getId_evento());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("evento", eventoDB);
        model.addAttribute("agendas", agendas);
        return "controlDeAsistencia";
    }

    @GetMapping("/cerrarEvento")
    public String cerrarEvento(Evento evento) {
        //eventoService.eliminar(evento);
        Evento eventoDB = eventoService.encontrarEvento(evento);
        eventoDB.setCerrado(Boolean.TRUE);

        List<Agenda> agendas = agendaService.buscarAgendaPorEvento(eventoDB.getId_evento());
        for (Agenda ag : agendas) {
            ClaseAlumno claseAlumno = claseAlumnoService.buscarPorAlumnoYCurso(ag.getAlumno().getId_usuario(),
                    eventoDB.getCursos().get(0).getId_curso());
            if (claseAlumno != null) {
                claseAlumno.setClases_tomadas(claseAlumno.getClases_tomadas() + 1);
            }

        }

        eventoService.guardar(eventoDB);
        return "redirect:/controlDeAsistencia?id_evento=" + eventoDB.getId_evento();
    }

    @GetMapping("/pagoInscripcion")
    @SuppressWarnings("empty-statement")
    public String pagoInscripcion(Cuenta cuenta,
            Model model,
            @AuthenticationPrincipal User user,
            @RequestParam(value = "keyword", required = false, defaultValue = "") String keyword) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());
        String[] rolesDeUsuario = {"ROLE_ALUMNO"};
        var alumnos = usuarioService.listarUsuarioPorRoles(rolesDeUsuario);

        String[] rolVendedor = {"ROLE_VENDEDOR"};
        var vendedores = usuarioService.listarUsuarioPorRoles(rolVendedor);

        var cursos = cursoService.listarCursosActivos();

        var cuentas = cuentaService.listarCuentasDeInscripcionSinPagar(keyword);

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("cursos", cursos);
        model.addAttribute("alumnos", alumnos);
        model.addAttribute("vendedores", vendedores);
        model.addAttribute("cuentas", cuentas);
        model.addAttribute("cursos", cursos);
        model.addAttribute("keyword", keyword);

        return ("pagoInscripcion");

    }

    @PostMapping("/guardarCuenta")
    public String guardarCalendario(Cuenta cuenta,
            @AuthenticationPrincipal User user) throws ParseException {

        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        Date fechaLimiteDePago = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(fechaLimiteDePago);
        cal.add(Calendar.DAY_OF_MONTH, 10);
        fechaLimiteDePago = cal.getTime();

        Date fechaVigencia = new Date();
        cal.setTime(fechaVigencia);
        cal.add(Calendar.MONTH, 6);
        fechaVigencia = cal.getTime();

        cuenta.setMonto_cuenta(cuenta.getCurso().getCosto_inscripcion());
        cuenta.setLiberado(Boolean.FALSE);
        cuenta.setUsuarioCreador(usuariologeado);
        cuenta.setEs_tipo_inscripcion(Boolean.TRUE);

        cuenta.setFecha_limite_de_pago(fechaLimiteDePago);
        cuenta.setVigencia(fechaVigencia);
        cuentaService.guardar(cuenta);

        return ("redirect:/pagoInscripcion");
    }

    @GetMapping("/misDatos")
    public String misDatos(Model model, @AuthenticationPrincipal User user) {
        Usuario usuariologeado;
        usuariologeado = usuarioService.listarPropiedadesDeUsuario(user.getUsername());

        model.addAttribute("usuarioImagen", usuariologeado.getfoto());
        model.addAttribute("usuario", usuariologeado);

        return ("misDatos");
    }

    @PostMapping("/guardarMisDatos")
    public String guardarMisDatos(@Valid Usuario usuario,
            @RequestParam("file") MultipartFile imagen,
            Model model) {

        Usuario usuarioDB = usuarioService.buscarUsuario(usuario);
        usuarioDB.setInscripciones(inscripcionService.buscarInscripcionesPorAlumno(usuario.getId_usuario()));

        if (!imagen.isEmpty()) {
            Path directorioImagenes = Paths.get("src//main//resources//static/imagenes");
            String rutaAbsoluta = directorioImagenes.toFile().getAbsolutePath();
            try {
                byte[] bytesImg = imagen.getBytes();
                Path rutaCompleta = Paths.get(rutaAbsoluta + "//" + imagen.getOriginalFilename());
                Files.write(rutaCompleta, bytesImg);
                usuarioDB.setfoto(imagen.getOriginalFilename());
            } catch (IOException e) {
                log.error(e.getMessage());
            }

        }

        usuarioDB.setPassword(usuario.getPassword());
        usuarioDB.setTelefono(usuario.getTelefono());
        usuarioDB.setCorreo(usuario.getCorreo());
        usuarioDB.setNombre_contacto(usuario.getNombre_contacto());
        usuarioDB.setTelefono_contacto(usuario.getTelefono_contacto());

        usuarioService.guardarUsuario(usuarioDB);
        return "redirect:/";
    }
}
