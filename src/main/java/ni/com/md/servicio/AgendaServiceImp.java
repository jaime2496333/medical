/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import java.util.Date;
import ni.com.md.dao.AgendaDao;
import ni.com.md.dao.EventoDao;
import ni.com.md.domain.Agenda;
import ni.com.md.domain.Evento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lenovo-T430-DT
 */
@Service
public class AgendaServiceImp implements AgendaService {

    @Autowired
    public AgendaDao agendaDao;
    @Autowired
    public EventoDao eventoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> listarAgendas() {
        return (List<Agenda>) agendaDao.findAll();
    }

    @Override
    @Transactional

    public void guardar(Agenda agenda) {
        Evento eventoDB = eventoDao.findById(agenda.getEvento().getId_evento()).orElse(null);

        if (eventoDB != null) {
            if (agenda.getId_agenda() == null) {
                int alumnosInscritos = eventoDB.getAlumnos_inscritos();
                eventoDB.setAlumnos_inscritos(alumnosInscritos + 1);
            }
            agendaDao.save(agenda);
            eventoDao.save(eventoDB);
        }
    }

    @Override
    @Transactional

    public void eliminar(Agenda agenda) {
        agendaDao.delete(agenda);
    }

    @Override
    @Transactional(readOnly = true)
    public Agenda encontrarAgenda(Agenda agenda) {
        return agendaDao.findById(agenda.getId_agenda()).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> buscarAgendaPorEventoYAlumno(Long id_usuario, Long id_evento) {
        return agendaDao.buscarAgendaPorEventoYAlumno(id_usuario, id_evento);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> buscarAgendasPorAlumno(Long id_usuario, Date fecha_evento) {
        return agendaDao.buscarAgendasPorAlumno(id_usuario, fecha_evento);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> buscarAgendaPorEvento(Long id_evento) {
        return agendaDao.buscarAgendaPorEvento(id_evento);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Agenda> buscarAgendaSinUsar(Long id_usuario) {
        return agendaDao.buscarAgendaSinUsar(id_usuario);
    }

}
