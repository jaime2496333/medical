package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.NivelDao;
import ni.com.md.dao.TemaDao;
import ni.com.md.domain.Nivel;
import ni.com.md.domain.Tema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TemaServiceImp implements TemaService {

    @Autowired
    private TemaDao temaDao;

    @Autowired
    private NivelDao nivelDao;

    @Override
    @Transactional(readOnly = true)
    public List<Tema> listartemas() {
        return (List<Tema>) temaDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Tema tema) {
        tema.setActivo(Boolean.TRUE);
        temaDao.save(tema);
    }

    @Override
    @Transactional
    public void guardarTema(Tema tema, Long id_nivel) {
        Nivel nivelDB = nivelDao.findById(id_nivel).orElse(null);
        if (nivelDB != null) {
            tema.setNivel(nivelDB);
            tema.setActivo(Boolean.TRUE);
            temaDao.save(tema);
        }
    }

    @Override
    @Transactional
    public void eliminar(Tema tema) {
                Tema temaDB = temaDao.findById(tema.getId_tema()).orElse(null);
        if (temaDB != null) {
            temaDB.setActivo(Boolean.FALSE);
            temaDao.save(temaDB);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Tema encontrarTema(Tema tema) {
        return temaDao.findById(tema.getId_tema()).orElse(null);
        //return temaDao.findById(tema.getId_tema()).orElse(null);
    }
 
    
    
    @Override
    @Transactional(readOnly = true)
    public List<Tema> listarTemasActivosPorCurso(Long id_curso) {
        return temaDao.listarTemasActivosPorCurso(id_curso);
    }

    @Override
    public List<Tema> listarTPorNivel(Long id_Nivel) {
return temaDao.listarTPorNivel(id_Nivel);
    }

}
