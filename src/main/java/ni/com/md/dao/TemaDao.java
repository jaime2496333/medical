package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Tema;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface TemaDao extends CrudRepository<Tema, Long>{
    
    @Query("select t,n,c from Tema t "
            + "join fetch t.nivel n "
            + "join fetch n.curso c "
            + "where t.activo=1 and c.id_curso=?1 "
            + "order by t.id_tema")
    public List<Tema> listarTemasActivosPorCurso(Long id_curso);
    
    
    @Query("select t,n,c from Tema t "
            + "join fetch t.nivel n "
            + "join fetch n.curso c "
            + "where t.activo=1 and n.id_nivel=?1 "
            + "order by t.id_tema")
    public List<Tema> listarTPorNivel(Long id_Nivel);
   
}
