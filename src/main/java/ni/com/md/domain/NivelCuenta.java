package ni.com.md.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "niveles_cuenta")

public class NivelCuenta implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_nivel_cuenta;
    
    private Boolean liberado;
    
    
    @ManyToOne
    @JoinColumn(name = "id_nivel")
    private Nivel nivel;
    
    @ManyToOne
    @JoinColumn(name = "id_inscripcion")
    private Inscripcion inscripcion;


}
