/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.Date;
import java.util.List;
import ni.com.md.dao.EventoDao;
import ni.com.md.domain.Evento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lenovo-T430-DT
 */
@Service

public class EventoServiceImp implements EventoService {

    @Autowired
    public EventoDao eventoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Evento> listarEventos() {
        return (List<Evento>) eventoDao.listarEventos();
    }

    @Override
    @Transactional

    public void guardar(Evento evento) {
        eventoDao.save(evento);
    }

    @Override
    @Transactional

    public void eliminar(Evento evento) {
        eventoDao.delete(evento);
    }

    @Override
    @Transactional(readOnly = true)
    public Evento encontrarEvento(Evento evento) {
        return eventoDao.findById(evento.getId_evento()).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Evento> listarEventosPorCursoPagadoDelAlumno(Long[] cursosPagados, Date fechaInicio, Date fechaFin, String keyword) {
        return eventoDao.listarEventosPorCursoPagadoDelAlumno(cursosPagados, fechaInicio, fechaFin, keyword);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean comprobarCupoEvento(Evento evento) {
        Evento eventoDB = eventoDao.findById(evento.getId_evento()).orElse(null);
        if (eventoDB != null) {
            if (eventoDB.getCupo_maximo() == eventoDB.getAlumnos_inscritos()) {
                return Boolean.FALSE;
            } else {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Override
    @Transactional(readOnly = true)
    public Evento buscarPorId(Long id_evento) {
        return eventoDao.findById(id_evento).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Evento> listarEventosPorFecha(Date inicio, Date fin, Long id_ubicacion) {
        return eventoDao.listarEventosPorFecha(inicio, fin, id_ubicacion);
    }

    @Override
    public List<Evento> listarEventosPorProfesor(Date inicio, Date fin, Long id_profesor) {
        return eventoDao.listarEventosPorProfesor(inicio, fin, id_profesor);
    }

    @Override
    public List<Evento> listarEventosFuturos(Date inicio, Long id_calendario) {
return eventoDao.listarEventosFuturos(inicio, id_calendario);
    }

}
