/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Ubicacion;

/**
 *
 * @author julio
 */
public interface UbicacionService {

    public List<Ubicacion> listarUbicaciones();

    public void guardar(Ubicacion ubicacion);

    public void eliminar(Ubicacion ubicacion);

    public Ubicacion encontrarUbicacion(Ubicacion ubicacion);

    public List<Ubicacion> listarUbicacionesActivas(String term);
}
