/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;
import java.util.List;
import ni.com.md.domain.Division;

public interface DivisionService {
     public List<Division>listarDivisiones();
    
    public void guardar (Division division);
    
    public void eliminar (Division division);
    
    public Division encontrarDivision(Division division); 
    
    public List<Division> listarDivisionesActivas();
}

