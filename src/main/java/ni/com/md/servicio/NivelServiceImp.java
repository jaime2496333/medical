/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.CursoDao;
import ni.com.md.dao.NivelDao;
import ni.com.md.domain.Curso;
import ni.com.md.domain.Nivel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NivelServiceImp implements NivelService {

    @Autowired
    private NivelDao nivelDao;

    @Autowired
    private CursoDao cursoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Nivel> listarNiveles() {
        return (List<Nivel>) nivelDao.findAll();
    }

    @Override
    @Transactional
    public void guardarNivel(Nivel nivel, Long id_curso) {
        Curso curso = cursoDao.findById(id_curso).orElse(null);
        if (curso != null) {
            nivel.setActivo(Boolean.TRUE);
            nivel.setCurso(curso);
            nivelDao.save(nivel);
        }
    }

    @Override
    public void guardar(Nivel nivel) {
        nivelDao.save(nivel);
    }

    @Override
    @Transactional
    public void eliminar(Nivel nivel) {
        Nivel nivelDB = nivelDao.findById(nivel.getId_nivel()).orElse(null);
        if (nivelDB != null) {
            nivelDB.setActivo(Boolean.FALSE);
            nivelDao.save(nivelDB);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Nivel encontrarNivel(Nivel nivel) {
        return nivelDao.buscarNivelPorId(nivel.getId_nivel());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Nivel> listarNivelesActivosPorCurso(Long id_curso) {
        return nivelDao.listarNivelesActivosPorCurso(id_curso);

    }

    @Override
    public Nivel buscarNivelPorId(Long id_nivel) {
        return nivelDao.buscarNivelPorId(id_nivel);
    }

    @Override
    public List<Nivel> listarNivelesActivos() {
return nivelDao.listarNivelesActivos();
    }
}
