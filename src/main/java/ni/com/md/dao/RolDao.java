/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.dao;
import ni.com.md.domain.Rol;
import org.springframework.data.repository.CrudRepository;

public interface RolDao extends CrudRepository<Rol, Long> {
    
}
