/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import ni.com.md.dao.AgendaDao;
import ni.com.md.dao.CalendarioDao;
import ni.com.md.dao.ClaseAlumnoDao;
import ni.com.md.dao.EventoDao;
import ni.com.md.domain.Agenda;
import ni.com.md.domain.Calendario;
import ni.com.md.domain.ClaseAlumno;
import ni.com.md.domain.Curso;
import ni.com.md.domain.Evento;
import ni.com.md.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Lenovo-T430-DT
 */
@Service
public class CalendarioServiceImp implements CalendarioService {

    @Autowired
    private CalendarioDao calendarioDao;
    @Autowired
    private EventoDao eventoDao;
    @Autowired
    private AgendaDao agendaDao;
    @Autowired
    private ClaseAlumnoDao claseAlumnoDao;

    @Override
    @Transactional(readOnly = true)
    public List<Calendario> listarCalendarios() {
        return (List<Calendario>) calendarioDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Calendario calendario, Usuario usuario) {

        if (calendario.getId_calendario() == null) {
            Long frecuencia = calendario.getFrecuencia();
            Date fechaInicial = calendario.getFecha_inicio();
            List<Evento> eventosDelCalendario = new ArrayList<>();
            while (fechaInicial.before(calendario.getFecha_fin())) {
                Calendar c = Calendar.getInstance();
                List<Curso> cursoDelEvento = new ArrayList<>();
                cursoDelEvento.add(calendario.getCurso());
                c.setTime(fechaInicial);
                Evento evento = new Evento();
                evento.setFecha_hora(fechaInicial);
                evento.setCupo_maximo(calendario.getCupo_maximo());
                evento.setCursos(cursoDelEvento);
                evento.setNumero_modelos(calendario.getNumero_modelos());
                evento.setUbicacion(calendario.getUbicacion());
                evento.setPublicado(calendario.getPublicado());
                evento.setUsuario(usuario);

                c.add(Calendar.DATE, frecuencia.intValue());
                fechaInicial = c.getTime();
                eventoDao.save(evento);

                eventosDelCalendario.add(evento);

            }
            calendario.setEventos(eventosDelCalendario);
            calendarioDao.save(calendario);
        } else {
            Long frecuencia = calendario.getFrecuencia();
            Date fechaInicial = calendario.getFecha_inicio();
            List<Evento> eventosDelCalendario = new ArrayList<>();

            Calendario calendarioDB = calendarioDao.listarEventosFuturos(calendario.getId_calendario(), new Date());
            eventosDelCalendario = eventoDao.listarEventosFuturos(new Date(), calendarioDB.getId_calendario());
            for (Evento eventoFuturo : calendarioDB.getEventos()) {

                List<Agenda> agendasFuturas = agendaDao.buscarAgendaPorEvento(eventoFuturo.getId_evento());
                for (Agenda agenda : agendasFuturas) {
                    ClaseAlumno claseAlumno = claseAlumnoDao.buscarPorAlumnoYCurso(agenda.getAlumno().getId_usuario(),
                            calendarioDB.getCurso().getId_curso());
                    claseAlumno.setClases_tomadas(claseAlumno.getClases_tomadas() - 1);
                    claseAlumnoDao.save(claseAlumno);
                }
                eventoDao.delete(eventoFuturo);
            }
            eventosDelCalendario = new ArrayList<>();
            while (fechaInicial.before(calendario.getFecha_fin())) {
                Calendar c = Calendar.getInstance();
                List<Curso> cursoDelEvento = new ArrayList<>();
                cursoDelEvento.add(calendario.getCurso());
                c.setTime(fechaInicial);
                Evento evento = new Evento();
                evento.setFecha_hora(fechaInicial);
                evento.setCupo_maximo(calendario.getCupo_maximo());
                evento.setCursos(cursoDelEvento);
                evento.setNumero_modelos(calendario.getNumero_modelos());
                evento.setUbicacion(calendario.getUbicacion());
                evento.setPublicado(calendario.getPublicado());

                evento.setUsuario(usuario);

                c.add(Calendar.DATE, frecuencia.intValue());
                fechaInicial = c.getTime();
                eventoDao.save(evento);
                eventosDelCalendario.add(evento);
            }
            calendario.setEventos(eventosDelCalendario);
            calendarioDB = this.encontrarCalendario(calendario);
            for (Evento evt : calendario.getEventos()) {
                evt.setActivo(calendarioDB.getActivo());
            }
            eventoDao.saveAll(calendario.getEventos());
            calendarioDao.save(calendario);
        }
    }

    @Override
    @Transactional
    public void eliminar(Calendario calendario) {
        for (Evento evento : calendario.getEventos()) {
            eventoDao.delete(evento);
        }
        calendarioDao.delete(calendario);
    }

    @Override
    @Transactional(readOnly = true)
    public Calendario encontrarCalendario(Calendario calendario) {
        return calendarioDao.findById(calendario.getId_calendario()).orElse(null);
    }

    @Override
    public List<Calendario> listarEventosPorUbicacion(Long id_ubicacion) {
        return calendarioDao.listarEventosPorUbicacion(id_ubicacion);
    }

}
