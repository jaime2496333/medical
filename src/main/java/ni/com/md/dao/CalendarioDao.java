/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;


import java.util.List;
import java.util.Date;
import ni.com.md.domain.Calendario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface CalendarioDao extends CrudRepository<Calendario, Long> {
    @Query("select c from Calendario c join fetch c.ubicacion ub where ub.id_ubicacion = ?1 and c.activo=1")
    public List<Calendario> listarEventosPorUbicacion(Long id_ubicacion);
    
    
    @Query("select c from Calendario c join fetch c.eventos e  where c.id_calendario = ?1 and e.fecha_hora > ?2")
    public Calendario listarEventosFuturos(Long id_calendario, Date inicio);
    
    
}
