package ni.com.md.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="tema")
public class Tema {
    
    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_tema;
    
    private String nombre_tema;

    private String descripcion_tema;
    
    private Boolean activo;

    @ManyToOne
    @JoinColumn(name="id_nivel")
    private Nivel nivel;
}
