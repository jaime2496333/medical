/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import ni.com.md.domain.Abono;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface AbonoDao extends CrudRepository<Abono, Long>{
    
    
}
