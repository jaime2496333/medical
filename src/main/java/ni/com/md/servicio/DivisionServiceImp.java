/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.dao.DivisionDao;

import ni.com.md.domain.Division;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DivisionServiceImp implements DivisionService {

    @Autowired
    private DivisionDao divisionDao;



    @Override
    @Transactional

    public void guardar(Division division) {
        division.setActivo(Boolean.TRUE);
        divisionDao.save(division);
    }

    @Override
    @Transactional

    public void eliminar(Division division) {
        Division divisionDB = divisionDao.findById(division.getId_division()).orElse(null);
        if (divisionDB != null) {
            divisionDB.setActivo(Boolean.FALSE);
            divisionDao.save(divisionDB);
        }

    }

    @Override
    @Transactional(readOnly = true)

    public Division encontrarDivision(Division division) {
        // return categoriaDao.findById(categoria.getId_categoria()).orElse(null);
        Division divisionDB = divisionDao.findById(division.getId_division()).orElse(null);
        if (divisionDB == null) {
            return null;
        }
        return divisionDB;
    }

    @Override
    @Transactional(readOnly = true)

    public List<Division> listarDivisiones() {
       return (List<Division>) divisionDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)

    public List<Division> listarDivisionesActivas() {
      return divisionDao.listarDivisionesActivas();
    }

}
