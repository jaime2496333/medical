/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "evento")
public class Evento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_evento;
    private Long cupo_maximo;
    private int alumnos_inscritos;
    private Long numero_modelos;
    
    private Date fecha_hora;
    
    private Boolean publicado;
    private Date creado_en;
    private Boolean cerrado;
    private Boolean activo;

    @PrePersist
    public void fechaDefault() {
        creado_en = new Date();
        alumnos_inscritos = 0;
        cerrado = Boolean.FALSE;
        activo = Boolean.TRUE;

    }

    @ManyToOne
    @JoinColumn(name = "id_ubicacion")
    private Ubicacion ubicacion;

    @ManyToOne
    @JoinColumn(name = "creado_por")
    private Usuario usuario;

     @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "curso_evento",
            joinColumns = @JoinColumn(name = "id_evento", insertable = false, updatable = false),
            inverseJoinColumns = @JoinColumn(name = "id_curso")
    )
    private List<Curso> cursos;
     
       @ManyToOne
    @JoinColumn(name = "id_maestro_asignado")
    private Usuario maestro;
}
