/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Ubicacion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author julio
 */
@Repository

public interface UbicacionDao extends CrudRepository<Ubicacion, Long> {
       @Query("select u from Ubicacion u where u.activo=1 and u.nombre_ubicacion like %?1% order by u.nombre_ubicacion")
    public List<Ubicacion> listarUbicacionesActivas(String term);
    
}
