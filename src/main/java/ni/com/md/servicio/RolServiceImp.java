/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.dao.RolDao;

import ni.com.md.domain.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RolServiceImp implements RolService {

    @Autowired
    private RolDao rolDao;

    @Override
    @Transactional(readOnly = true)
    public List<Rol> listarRoles() {
        return (List<Rol>) rolDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(Rol rol) {
        rolDao.save(rol);
    }

    @Override
    @Transactional

    public void eliminar(Rol rol) {
        rolDao.delete(rol);
    }

    @Override
    @Transactional(readOnly = true)
    public Rol encontrarRol(Rol rol) {
        return rolDao.findById(rol.getId_rol()).orElse(null);
    }

}
