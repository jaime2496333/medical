package ni.com.md.servicio;

import java.util.List;

import ni.com.md.dao.CursoDao;
import ni.com.md.domain.Curso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoServiceImp implements CursoService {
    
    @Autowired
    private CursoDao cursoDao;
    
    @Autowired
    private CategoriaService categoriaService;
    
    @Override
    @Transactional(readOnly = true)
    public List<Curso> listarCursos() {
        return (List<Curso>) cursoDao.findAll();
    }
    
    @Override
    @Transactional
    public void guardar(Curso curso) {
        
        cursoDao.save(curso);
    }
    
    @Override
    @Transactional
    public void eliminar(Curso curso) {
        cursoDao.delete(curso);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Curso encontrarCurso(Curso curso) {
        Curso cursoDB = cursoDao.findById(curso.getId_curso()).orElse(null);
        if (cursoDB == null) {
            return null;
        }
        return cursoDB;
    }
    
    @Override
    public List<Curso> listarCursosActivos() {
        return (List<Curso>) cursoDao.listarCursosActivos();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Curso encontrarCursoPorId(Long id_curso) {
        return cursoDao.findById(id_curso).orElse(null);
    }
    
}
