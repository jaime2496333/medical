/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.md.servicio;

import java.util.List;

import ni.com.md.domain.Inscripcion;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface InscripcionService {

    public List<Inscripcion> listarInscripcion();

    public void guardar(Inscripcion inscripcion);

    public void eliminar(Inscripcion inscripcion);

    public Inscripcion encontrarInscripcion(Inscripcion inscripcion);

    public Inscripcion encontrarInscripcionbyId(Long id_inscripcion);

    public List<Inscripcion> listarInscripcionActivas();

    public List<Inscripcion> listarInscripcionActivasPorCursoNivel(Long id_curso, String term);

    public List<Inscripcion>  buscarInscripcionPorUsuarioYNivel(Long id_usuario, Long[] niveles);
    
    public List<Inscripcion> listarInscripcionesSinPagar(String term);
    
    public List<Inscripcion> buscarInscripcionesPagadasPorAlumno(Long id_alumno);
    
    public List<Inscripcion> buscarInscripcionesPorAlumno(Long id_alumno);
    
    public List<Inscripcion> buscarInscripcionPorUsuarioYCurso(Long id_usuario, Long id_curso);

}
