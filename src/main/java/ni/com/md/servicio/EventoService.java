/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.Date;
import java.util.List;

import ni.com.md.domain.Evento;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface EventoService {
    
    public List<Evento> listarEventos();
    
    public void guardar (Evento evento);
    
    public void eliminar (Evento evento);
    
    public Evento encontrarEvento(Evento evento); 
    
    public List<Evento> listarEventosPorCursoPagadoDelAlumno(Long[] cursosPagados, Date fechaInicio, Date fechaFin, String keyword);
    
    public List<Evento> listarEventosPorFecha(Date inicio, Date fin, Long id_ubicacion);
   
    public Boolean comprobarCupoEvento(Evento evento);
    
    public Evento buscarPorId(Long id_evento);
    
    public List<Evento> listarEventosPorProfesor(Date inicio, Date fin, Long id_profesor);
    
    public List<Evento> listarEventosFuturos(Date inicio, Long id_calendario);
}
