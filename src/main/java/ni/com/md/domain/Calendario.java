/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "calendario")
public class Calendario implements Serializable {
     private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_calendario;
    
    private String descripcion_calendario;
    
    private Date fecha_inicio;
    
    private Date fecha_fin;
    
    private Long frecuencia;
    
    private Long cupo_maximo;
    
    private Long numero_modelos;
    
    private Boolean publicado;
    
    private Boolean activo;
    
    @PrePersist
    public void activo_true() {
        activo = Boolean.TRUE;
    }
    
    @ManyToOne
    @JoinColumn(name="id_nivel")
    private Nivel nivel;
    
    @ManyToOne
    @JoinColumn(name="id_curso")
    private Curso curso;
    
     @ManyToOne
    @JoinColumn(name="id_ubicacion")
    private Ubicacion ubicacion;
    
    @OneToMany
    @JoinColumn(name="id_calendario")
    private List<Evento> eventos;

}
