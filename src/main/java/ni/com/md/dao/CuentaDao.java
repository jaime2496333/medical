/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Cuenta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author julio
 */
public interface CuentaDao extends CrudRepository<Cuenta, Long> {

    @Query("select c from Cuenta c "
            + "join fetch c.alumno a "
            + "where c.liberado = 0 and c.es_tipo_inscripcion is null and "
            + "(a.username like %?1% or "
            + "a.nombres like %?1% or "
            + "a.apellidos like %?1%)")
    public List<Cuenta> listarCuentasSinPagar(String term);
    
    @Query("select c from Cuenta c "
            + "join fetch c.alumno a "
            + "join fetch c.curso cur "
            + "where c.liberado = 0 and c.es_tipo_inscripcion = 1 and "
            + "(a.username like %?1% or "
            + "a.nombres like %?1% or "
            + "a.apellidos like %?1% or "
            + "cur.nombre_curso like %?1%)")
    public List<Cuenta> listarCuentasDeInscripcionSinPagar(String term);
    
    @Query("select c from Cuenta c "
            + "join fetch c.alumno a "
            + "join fetch c.curso cur "
            + "where c.liberado = 1 and c.es_tipo_inscripcion = 1 and "
            + "cur.id_curso=?1 and c.vigencia >= CURRENT_DATE")
    public List<Cuenta> listarCuentasPagadasPorCurso(Long id_curso);

}
