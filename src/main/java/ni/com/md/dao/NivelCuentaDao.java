/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import ni.com.md.domain.NivelCuenta;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author julio
 */
public interface NivelCuentaDao extends CrudRepository<NivelCuenta, Long> {
    
}
