/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.NivelCuentaDao;
import ni.com.md.domain.NivelCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author julio
 */
@Service
public class NivelCuentaServiceImpl implements NivelCuentaService {

    @Autowired
    private NivelCuentaDao nivelCuentaDao;

    @Override
    public List<NivelCuenta> listarNivelesCuentas() {
        return (List<NivelCuenta>) nivelCuentaDao.findAll();
    }

    @Override
    public void guardar(NivelCuenta nivelCuenta) {
        nivelCuentaDao.save(nivelCuenta);
    }

    @Override
    public void eliminar(NivelCuenta nivelCuenta) {
        nivelCuentaDao.delete(nivelCuenta);
    }

    @Override
    public NivelCuenta encontrarNivelCuenta(NivelCuenta nivelCuenta) {
        return nivelCuentaDao.findById(nivelCuenta.getId_nivel_cuenta()).orElse(null);
    }

}
