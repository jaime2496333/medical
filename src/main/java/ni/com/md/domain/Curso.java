package ni.com.md.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name = "curso")
public class Curso implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_curso;

    @NotEmpty
    private String nombre_curso;
 
    private String  imagen_curso;
    
    private Boolean activo;
    
    private float costo_inscripcion;
    
    private float comision_vendedor;
    
    @ManyToOne
    @JoinColumn(name="id_categoria")
    private Categoria categoria;
    
    @ManyToOne
    @JoinColumn(name="id_division")
    private Division division;
    
}
