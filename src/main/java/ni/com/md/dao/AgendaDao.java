/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.dao;

import java.util.List;
import ni.com.md.domain.Agenda;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Lenovo-T430-DT
 */
@Repository
public interface AgendaDao extends CrudRepository<Agenda, Long> {

    @Query("select a,e,u from Agenda a "
            + "join fetch a.evento e "
            + "join fetch a.alumno u "
            + "where u.id_usuario = ?1 and e.id_evento=?2")
    public List<Agenda> buscarAgendaPorEventoYAlumno(Long id_usuario, Long id_evento);

    @Query("select a,e,ub from Agenda a "
            + "join a.evento e "
            + "join a.alumno u "
            + "join e.ubicacion ub "
            + "where u.id_usuario = ?1  and e.publicado=1 and e.fecha_hora > ?2 order by e.fecha_hora")
    public List<Agenda> buscarAgendasPorAlumno(Long id_usuario, Date fecha_actual);
    
     @Query("select a,e,u from Agenda a "
            + "join fetch a.evento e "
            + "join fetch a.alumno u "
            + "where e.id_evento=?1")
    public List<Agenda> buscarAgendaPorEvento(Long id_evento);
    
    
    @Query("select a,e,u from Agenda a "
            + "join fetch a.evento e "
            + "join fetch a.alumno u "
            + "where u.id_usuario = ?1 and a.asistencia = 0")
    public List<Agenda> buscarAgendaSinUsar(Long id_usuario);


}
