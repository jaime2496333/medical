/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.Calendario;
import ni.com.md.domain.Usuario;

/**
 *
 * @author Lenovo-T430-DT
 */
public interface CalendarioService {
     public List<Calendario> listarCalendarios();
    
    public void guardar (Calendario calendario, Usuario usuario);
    
    public void eliminar (Calendario calendario);
    
    public Calendario encontrarCalendario(Calendario calendario);
    
    public List<Calendario> listarEventosPorUbicacion(Long id_ubicacion);
}
