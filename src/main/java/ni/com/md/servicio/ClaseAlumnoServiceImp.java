/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.dao.ClaseAlumnoDao;
import ni.com.md.domain.ClaseAlumno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author julio
 */
@Service
public class ClaseAlumnoServiceImp implements ClaseAlumnoService {

    @Autowired
    private ClaseAlumnoDao claseAlumnoDao;

    @Override
    @Transactional(readOnly = true)
    public List<ClaseAlumno> listarClaseAlumnos() {
        return (List<ClaseAlumno>) claseAlumnoDao.findAll();
    }

    @Override
    @Transactional
    public void guardar(ClaseAlumno claseAlumno) {
        claseAlumnoDao.save(claseAlumno);
    }

    @Override
    @Transactional
    public void eliminar(ClaseAlumno claseAlumno) {
        claseAlumnoDao.delete(claseAlumno);
    }

    @Override
    @Transactional(readOnly = true)
    public ClaseAlumno encontrarClaseAlumno(ClaseAlumno claseAlumno) {
        return claseAlumnoDao.findById(claseAlumno.getId_clase_alumno()).orElse(null);
    }

    @Override
    public ClaseAlumno buscarPorAlumnoYCurso(Long id_alumno, Long id_Curso) {
        return claseAlumnoDao.buscarPorAlumnoYCurso(id_alumno, id_Curso);
    }

    @Override
    public List<ClaseAlumno> buscarPorAlumno(Long id_alumno) {
        return claseAlumnoDao.buscarPorAlumno(id_alumno);
    }

}
