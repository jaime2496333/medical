/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package ni.com.md.servicio;

import java.util.List;
import ni.com.md.domain.NivelCuenta;

/**
 *
 * @author julio
 */
public interface NivelCuentaService {
    public List<NivelCuenta> listarNivelesCuentas();
    
    public void guardar (NivelCuenta nivelCuenta);
    
    public void eliminar (NivelCuenta nivelCuenta);
    
    public NivelCuenta encontrarNivelCuenta(NivelCuenta nivelCuenta); 
}
